package fr.unice.polytech.si3.qgl.iabc.map;

import java.awt.*;
import java.util.List;

/**
 * @author Maven
 * @version 1.3
 *          This class embodies tiles which are discovered during both exploring phases.
 *          Tile are separated in two types, tiles which contains ground, and tiles which only
 *          contains ocean biome.
 */
public class Tile {

    //the coordinate (x,y).
    private Point position;
    //to know whether it's a ground or not.
    private boolean isGround;
    //to know whether it's hazardous or not.
    private boolean isDanger;
    //to know whether it has already been explored or not.
    private boolean isExplored;
    //to know whether it has already been exploited or not.
    private boolean isScouted;
    //the id of a creek.
    private String idCreek = null;
    //the id of the emergency site.
    private String idEmergencySite = null;
    //the list of biomes.
    private List<Biomes> biomesList;
    //the list of resources.
    private List<Resources> resourcesList;
    //the list of conditions.
    private List<Conditions> conditionsList;
    //the list of amount.
    private List<Amount> amountList;

    /**
     * Constructor for a terrestrial tile.
     *
     * @param position   the coordinates of the tile.
     * @param isExplored to know whether the tile has been explored or not.
     * @param biomesList the tile's biome list.
     * @param isScouted  to know wheter the tile has been scouted or not.
     */
    public Tile(boolean isExplored, boolean isScouted, Point position, List<Biomes> biomesList) {
        this.position = position;
        this.isExplored = isExplored;
        this.biomesList = biomesList;
        this.isScouted = isScouted;
    }

    /**
     * Constructor of the class Tile.
     *
     * @param position      the coordinates of the tile.
     * @param resourcesList the list of resources.
     * @param isExplored    to know whether it's been explored or not.
     */
    public Tile(Point position, boolean isExplored, List<Resources> resourcesList, List<Conditions> conditionsList, List<Amount> amountList) {
        this.position = position;
        this.resourcesList = resourcesList;
        this.isExplored = isExplored;
        this.amountList = amountList;
        this.conditionsList = conditionsList;
    }

    /**
     * Basic constructor of the class Tile.
     * This constructor is dedicated for the aerial tiles
     * which are nor a creek, nor an emergency site.
     *
     * @param position   the coordinates of the tile.
     * @param isGround   to know whether there is ground or not.
     * @param isDanger   to know whether it's hazardous or not.
     * @param biomesList the tile's biome list.
     */
    public Tile(Point position, boolean isGround, boolean isDanger, List<Biomes> biomesList) {
        this.position = position;
        this.isGround = isGround;
        this.isDanger = isDanger;
        this.biomesList = biomesList;
    }

    /**
     * Constructor of the class Tile.
     * This constructor is made for particular aerial tiles which
     * contains either a creek, or an Emergency site, or both.
     *
     * @param position        the coordinates of a tile.
     * @param isGround        to know whether there is ground or not.
     * @param isDanger        to know whether it's hazardous or not.
     * @param idCreek         the id of the creek.
     * @param idEmergencySite the id of the emergency site.
     * @param biomesList      the list of the biomes.
     */
    public Tile(Point position, boolean isGround, boolean isDanger, String idCreek, String idEmergencySite, List<Biomes> biomesList) {
        this(position, isGround, isDanger, biomesList);
        this.idCreek = idCreek;
        this.idEmergencySite = idEmergencySite;
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the position x.
     *
     * @return int the position x.
     */
    public Point getPosition() {
        return position;
    }

    /**
     * Checking whether it's a ground or not.
     *
     * @return A boolean, true if the tile is not in the ocean, false otherwise.
     */
    public boolean isGround() {
        return isGround;
    }

    /**
     * Checking whether it's a dangerous tile or not.
     *
     * @return A boolean, true if the tile is dangerous, false otherwise.
     */
    public boolean isDanger() {
        return isDanger;
    }

    /**
     * Checks if the tile is explored.
     */
    public boolean isExplored() {
        return isExplored;
    }

    /**
     * Getter of the number/id of the creek.
     *
     * @return String the number/id of the creek.
     */
    public String idCreek() {
        return idCreek;
    }

    /**
     * Getter of the number/id of the emergency site.
     *
     * @return String the number/id of the emergency site.
     */
    public String idEmergencySite() {
        return idEmergencySite;
    }

    /**
     * Getter of the biomesList.
     *
     * @return List<Biomes> the biomesList.
     */
    public List<Biomes> getBiomesList() {
        return biomesList;
    }

    /**
     * Setter of the ground.
     *
     * @param newBool true if the tile contains ground, false otherwise.
     */
    public void setIsGround(boolean newBool) {
        isGround = newBool;
    }

    /**
     * Setter of the exploration.
     *
     * @param newBool true when it has already been explored.
     */
    public void setIsExplored(boolean newBool) {
        isExplored = newBool;
    }

    /**
     * Set whether the tile hae been scouted or not.
     *
     * @param newBool true when it has already been scouted.
     */
    public void setIsScouted(boolean newBool) {
        isScouted = newBool;
    }

    /**
     * Setter of the Id of the creek.
     *
     * @param newString the new id of the creek.
     */
    public void setIdCreek(String newString) {
        idCreek = newString;
    }

    /**
     * Setter of the id of the emergency site.
     *
     * @param newString the new id of the emergency site.
     */
    public void setIdEmergencySite(String newString) {
        idEmergencySite = newString;
    }

}
