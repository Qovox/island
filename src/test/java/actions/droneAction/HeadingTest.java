package actions.droneAction;

import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Heading;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class HeadingTest {

    private Drone drone;
    private Context context;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void headingToString() {
        String headingToString = "{\"action\":\"heading\"," +
                "\"parameters\":{\"direction\":\"N\"}}";
        String badHeadingToString = "{\"action\":\"heading\"," +
                "\"parameters\":{\"direction\":\"S\"}}";
        Heading heading = new Heading(Directions.NORTH);
        assertEquals(headingToString, heading.toString());
        assertNotEquals(headingToString, new Heading(Directions.SOUTH).toString());
        assertEquals("heading", heading.getName());
        assertNotEquals(badHeadingToString, heading.toString());
    }

    @Test
    public void headingAftermath() {
        String result = "{\"cost\":6}";

        new Heading(Directions.NORTH).aftermath(result, context, drone);
        assertEquals(19994, context.getBudget().getBudgetLeft());
        assertEquals(new Point(-1, 1), drone.getPosition());

        new Heading(Directions.EAST).aftermath(result, context, drone);
        assertEquals(19988, context.getBudget().getBudgetLeft());
        assertEquals(new Point(0, 2), drone.getPosition());
    }

}
