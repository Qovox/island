package map;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

public class TerrestrialMapTest {

    private fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap terrestrialMap;

    @Before
    public void setUp() {
        terrestrialMap = new fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap(new ArrayList<>());
    }

    @Test
    public void mapExploreTest() {
        assertTrue(terrestrialMap.getMap().isEmpty());
    }

}
