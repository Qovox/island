package fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Glimpse;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.MoveTo;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.map.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.0
 *          This is the part of the strategy which controls what to do
 *          after each action. It is also charged to interpret the last
 *          actions results.
 */
public class StrategyCommander {

    private int moveRange;
    private int glimpseLevel;
    private Point menPosition;
    private Radar radar;
    private TerrestrialMap terrestrialMap;
    private boolean isScouted = false;
    private boolean canExplore = false;
    private boolean canExploit = false;
    private boolean checkBeforeExplore = false;
    //the inventory of the men.
    private Inventory inventory;
    private Directions explorationDirection;
    private List<Action> actions;
    //the amount of resources present on a exploited tile.
    private List<Amount> amount;
    private List<Resources> resources;
    private List<Conditions> cond;

    /**
     * Constructor of StrategyCommander.
     *
     * @param position       the position of Men.
     * @param inventory      the inventory of Men.
     * @param terrestrialMap the map.
     */
    public StrategyCommander(Point position, Inventory inventory, TerrestrialMap terrestrialMap) {
        glimpseLevel = 1;
        menPosition = position;
        this.terrestrialMap = terrestrialMap;
        this.inventory = inventory;
        actions = new ArrayList<>();
    }

    public List<Action> takeDecision() {
        actions = exploreExploit();
        if (!actions.isEmpty())
            return actions;
        actions = scoutGlimpse();
        if (!actions.isEmpty())
            return actions;
        return new ArrayList<>();
    }

    /**
     * Treats explore and exploit actions
     *
     * @return the list of actions to do
     */
    public List<Action> exploreExploit() {
        if (canExploit) {
            canExploit = false;
            return new Exploitation(inventory, amount, resources, cond, terrestrialMap, menPosition).takeDecisionList();
        }
        if (canExplore) {
            canExplore = false;
            MoveAndExplore moveAndExplore = new MoveAndExplore(explorationDirection);
            actions = moveAndExplore.takeDecisionList();
            return actions;
        }
        if (checkBeforeExplore) {
            actions.add(new Glimpse(explorationDirection, 2));
            return actions;
        }
        return new ArrayList<>();
    }

    /**
     * Treats scout and glimpse actions
     *
     * @return the list of actions to do
     */
    public List<Action> scoutGlimpse() {
        if (moveRange != 0) {
            for (int i = 0; i < moveRange; i++)
                actions.add(new MoveTo(explorationDirection));
            moveRange = 0;
            return actions;
        }
        if (isScouted) {
            glimpseLevel++;
            if (glimpseLevel < 5)
                actions = new SearchSafeGround(terrestrialMap, menPosition, glimpseLevel).takeDecisionList();
            if (actions.isEmpty()) {
                boolean inDanger = false;
                if (terrestrialMap.getTileByPositionWithDirection(menPosition, explorationDirection, 1).isPresent() &&
                        terrestrialMap.getTileByPositionWithDirection(menPosition, explorationDirection, 1).get().getBiomesList().contains(Biomes.OCEAN)) {
                    explorationDirection = oppositeDir();
                    inDanger = true;
                }
                isScouted = false;
                glimpseLevel = 1;
                return new SetNewPlace(terrestrialMap, menPosition, radar, explorationDirection, inDanger).takeDecisionList();
            }
            return actions;
        }
        isScouted = true;
        actions = new ScoutAround(terrestrialMap, menPosition).takeDecisionList();
        if (actions.isEmpty()) {
            return scoutGlimpse();
        } else return actions;
    }

    /**
     * This method allows to know when we reach the OCEAN after an explore action.
     *
     * @param amount    the amount of resources.
     * @param resources the considered resource.
     * @param cond      the condtion of the exploring.
     */
    public void exploreResults(List<Amount> amount, List<Resources> resources, List<Conditions> cond) {
        this.amount = amount;
        this.resources = resources;
        this.cond = cond;
        canExploit = true;
    }

    /**
     * This method allows to consider only the fourth tile in order to know when we reach the Ocean or not.
     *
     * @param direction      the direction of the glimpse.
     * @param range          the range of the glimpse.
     * @param biomesGlimpsed the report.
     * @return true, if we found OCEAN at the fourth tile.
     */
    public boolean glimpseResults(Directions direction, int range, List<List<Biomes>> biomesGlimpsed) {
        if (!biomesGlimpsed.get(range - 1).contains(Biomes.OCEAN)) {
            explorationDirection = direction;
            glimpseLevel = 1;
            if (checkBeforeExplore) {
                checkBeforeExplore = false;
                canExplore = true;
                return true;
            }
            isScouted = false;
            moveRange = range - 1;
            return true;
        }
        if (checkBeforeExplore) {
            checkBeforeExplore = false;
            isScouted = true;
        }
        return false;
    }

    /**
     * This method allows to know when we reach the Ocean or not after a scout action.
     *
     * @param direction the considered direction.
     * @param resources the displayed resources.
     * @return True, if we found FISH, false otherwise.
     */
    public boolean scoutResults(Directions direction, List<Resources> resources) {
        for (Resources resource : resources) {
            if (inventory.getResourcesToCollect().contains(resource)) {
                explorationDirection = direction;
                isScouted = false;
                if (resource.equals(Resources.FISH)) {
                    checkBeforeExplore = true;
                    return true;
                }
                canExplore = true;
                return true;
            }
        }
        return false;
    }

    /**
     * This method allows to update the inventory, the map and the position of the men after an action.
     *
     * @param position       the position to update.
     * @param terrestrialMap the map to update.
     * @param inventory      the inventory to update.
     */
    public void updateAfterAction(Point position, TerrestrialMap terrestrialMap, Radar radar, Inventory inventory) {
        menPosition = position;
        this.radar = radar;
        this.terrestrialMap = terrestrialMap;
        this.inventory = inventory;
    }

    public Directions oppositeDir() {
        switch (explorationDirection) {
            case NORTH:
                return Directions.SOUTH;
            case EAST:
                return Directions.WEST;
            case SOUTH:
                return Directions.NORTH;
            case WEST:
                return Directions.EAST;
            default:
                return explorationDirection;
        }
    }


}
