package fr.unice.polytech.si3.qgl.iabc.actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Maven
 * @version 1.2
 *          This class represents the transform action.
 */
public class Transform extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;
    //the resources to transform.
    private Map<Resources, Integer> resourceList = new HashMap<>();

    /**
     * {"action":"transform","parameters":{"WOOD":6,"QUARTZ":11}}
     * Constructor of the class Transform.
     *
     * @param resourceList the list of the different resources.
     */
    public Transform(Map<Resources, Integer> resourceList) {
        name = "transform";
        JSONObject parameters = new JSONObject();
        List<Resources> resourceName = resourceList.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList());
        List<Integer> resourceIndex = resourceList.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
        for (int i = 0; i < resourceList.size(); i++) {
            parameters.put(resourceName.get(i).toString(), resourceIndex.get(i));
        }
        action = new JSONObject();
        action.put("action", name);
        action.put("parameters", parameters);
        this.resourceList = resourceList;
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param men     the previous men to update.
     */
    @Override
    public void aftermath(String result, Context context, Men men) {
        super.aftermath(result, context, men);
        JSONObject stringToJSON = new JSONObject(result);
        JSONObject extrasObjects = stringToJSON.getJSONObject("extras");
        int production = extrasObjects.getInt("production");
        Resources kind = Resources.getResourceName(extrasObjects.getString("kind"));
        men.transformUpdate(resourceList, production, kind);
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Getter of the list of resources and their amount.
     *
     * @return the map which contains the resources to transform.
     */
    public Map<Resources, Integer> getResourceList() {
        return resourceList;
    }

}
