package fr.unice.polytech.si3.qgl.iabc.map;

/**
 * @author Maven
 * @version 1.0
 *          Enumeration of the 4 directions.
 */
public enum Directions {

    NORTH("N"), SOUTH("S"), EAST("E"), WEST("W");

    private String name;

    /**
     * Constructor of an enum.
     *
     * @param name the name of the enum.
     */
    Directions(String name) {
        this.name = name;
    }

    /**
     * @return a string representation of the object name.
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * This method allows an Enum to get its real name.
     *
     * @return the real name of the Enum.
     */
    public static Directions getDirectionName(String direction) {
        for (Directions directions : Directions.values()) {
            if (directions.toString().equals(direction)) {
                return directions;
            }
        }
        return null;
    }

}
