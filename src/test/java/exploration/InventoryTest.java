package exploration;

import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by Thoma on 2/12/2017.
 *
 * @author Maven
 */
public class InventoryTest {

    private Inventory inventory;
    private Map<Resources, Integer> contract;

    @Before
    public void setUp() {
        contract = new HashMap<>();
        contract.put(Resources.FISH, 20);
        contract.put(Resources.FRUITS, 47);
        contract.put(Resources.WOOD, 50);
        contract.put(Resources.QUARTZ, 50);
        contract.put(Resources.GLASS, 2);
        inventory = new Inventory(contract);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void updateTest() {
        assertEquals(Resources.FISH, inventory.update(Resources.FISH, 15));
        assertNotEquals(Resources.FISH, inventory.update(Resources.FISH, 15));
        assertEquals(Resources.FRUITS, inventory.priorityResource());
    }

    @Test
    public void contractDontChange() {
        assertTrue(inventory.noRessourcesCollected());
        inventory.update(Resources.FISH, 1);
        assertFalse(inventory.noRessourcesCollected());
    }

    @Test
    public void checkTransform() {
        Map<Resources, Integer> expected = new HashMap<>();
        expected.put(Resources.WOOD, 7);
        expected.put(Resources.QUARTZ, 12);
        inventory.update(Resources.QUARTZ, 40);
        inventory.update(Resources.WOOD, 40);
        assertEquals(expected.toString(), inventory.checkTransform().toString());
    }

}



