package actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.menAction.MoveTo;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Transform;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TransformTest {

    private Men men;
    private Context context;
    private Drone drone;

    @Before
    public void setUp() {        String stringContext = "{\"men\": 10,\"budget\":20000,\"contracts\":" +
            "[{ \"amount\":6000,\"resource\":\"WOOD\"}," +
            "{ \"amount\":2000,\"resource\":\"GLASS\"}]," +
            "\"heading\":\"W\"}";
        men = new Men(new Point(),new Inventory(new HashMap<>()), new Radar());
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void transformToString() {
        Map<Resources, Integer> resourceslist = new HashMap<>();
        resourceslist.put(Resources.WOOD, 1000);
        resourceslist.put(Resources.FUR, 300);

        JSONObject parameters = new JSONObject();
        List<Resources> resourceName = resourceslist.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList());
        List<Integer> resourceIndex = resourceslist.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
        for (int i = 0; i < resourceslist.size(); i++) {
            parameters.put(resourceName.get(i).toString(), resourceIndex.get(i));
        }
        JSONObject action = new JSONObject();
        action.put("action", "transform");
        action.put("parameters", parameters);

        Transform transform = new Transform(resourceslist);
        String badTransformToString = "{\"action\":\"transformme\",\"parameters\":{\"WOODS\":900,\"QUARTZ\":11}}";
        assertEquals(action.toString(), transform.toString());
        assertNotEquals(badTransformToString, transform.toString());
        assertEquals("transform", transform.getName());
        assertEquals(resourceslist, transform.getResourceList());
    }

    @Ignore
    public void transformAftermath(){
        Map<Resources, Integer> desRessources = new HashMap<>();
        desRessources.put(Resources.WOOD, 20);
        desRessources.put(Resources.FUR, 30);
        String result = "{ \"cost\": 8, \"extras\": { \"production\": 1, \"kind\": \"GLASS\" },\"status\": \"OK\" }";
        new Transform(desRessources).aftermath(result, context, men);
        assertEquals(19992, context.getBudget().getBudgetLeft());



    }

}
