package actions.droneAction;

import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Echo;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class EchoTest {

    private Drone drone;
    private Context context;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void echoToString() {
        String echoToString = "{\"action\":\"echo\"," +
                "\"parameters\":{\"direction\":\"S\"}}";
        String badEchoToString = "{\"action\":\"echo\"," +
                "\"parameters\":{\"direction\":\"N\"}}";
        Echo echo = new Echo(Directions.SOUTH);
        assertEquals(echoToString, echo.toString());
        assertNotEquals(echoToString, new Echo(null).toString());
        assertEquals("echo", echo.getName());
        assertNotEquals(badEchoToString, echo.toString());
    }

    @Test
    public void echoAftermathGround() {
        String result = "{\"cost\":10,\"extras\":{\"range\":2,\"found\":\"GROUND\"}}";

        new Echo(Directions.NORTH).aftermath(result, context, drone);
        assertEquals(19990, context.getBudget().getBudgetLeft());
        assertEquals(true, context.getMap().getTileByPosition(new Point(0, 2)).get().isGround());
        assertEquals(true, context.getMap().getTileByPosition(new Point(0, 1)).isPresent());
        assertEquals(true, context.getMap().getTileByPosition(new Point(0, 2)).isPresent());
    }

    @Test
    public void echoAftermathOoR() {
        String result = "{\"cost\":7,\"extras\":{\"range\":52,\"found\":\"OUT_OF_RANGE\"}}";
        new Echo(Directions.SOUTH).aftermath(result, context, drone);
        assertEquals(19993, context.getBudget().getBudgetLeft());
        for (int i = 0; i < 2; i++) {
            assertEquals(true, context.getMap().getTileByPosition(new Point(0, -51 - i)).get().isDanger());
        }
    }

}
