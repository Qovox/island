package fr.unice.polytech.si3.qgl.iabc.actions.droneAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import org.json.JSONObject;

/**
 * @author Maven
 * @version 1.2
 *          This class implements the fly action behavior.
 */
public class Fly extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;

    /**
     * {"action":"fly"}
     * Constructor of the class fly.
     */
    public Fly() {
        name = "fly";
        action = new JSONObject();
        action.put("action", name);
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param drone   the previous drone to update.
     */
    @Override
    public void aftermath(String result, Context context, Drone drone) {
        super.aftermath(result, context, drone);
        context.flyUpdate(drone);
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

}
