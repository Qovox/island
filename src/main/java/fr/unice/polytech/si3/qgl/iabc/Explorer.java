package fr.unice.polytech.si3.qgl.iabc;

import eu.ace_design.island.bot.IExplorerRaid;
import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.Land;
import fr.unice.polytech.si3.qgl.iabc.actions.Stop;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;

public class Explorer implements IExplorerRaid {

    private Men men;
    private Drone drone;
    private Context context;
    //the last realized action.
    private Action lastAction;
    private String landingCreek;
    //to know whether it's the terrestrial(ground) phase or not.
    private boolean groundPhase = false;

    /**
     * This method allows to initialize the game(drone, map, budget, feedback).
     *
     * @param context the initial context of the game.
     */
    @Override
    public void initialize(String context) {
        drone = new Drone();
        this.context = new Context(context, drone);
    }

    /**
     * This method allows to execute the action decided by the drone.
     *
     * @return The action to be done.
     */
    @Override
    public String takeDecision() {
        try {
            if (!context.check()) {
                lastAction = new Stop();
                return lastAction.toString();
            }
            if (groundPhase) {
                lastAction = men.takeDecision();
                return lastAction.toString();
            }
            if (drone.getIsBack()) {
                groundPhase = true;
                men = new Men(drone.getPosition(), context.getInventory(), drone.getRadar());
                landingCreek = context.getMap().getCreeksList().get(0).idCreek();
                return new Land(landingCreek, context.nbrOfMenToUse()).toString();
            }
            lastAction = drone.takeDecision();
            return lastAction.toString();
        } catch (Exception e) {
            lastAction = new Stop();
            return lastAction.toString();
        }
    }

    /**
     * This method allows to get the information related to each action realized by the drone.
     *
     * @param results the feedback from the last action.
     */
    @Override
    public void acknowledgeResults(String results) {
        if (groundPhase) {
            lastAction.aftermath(results, context, men);
        } else lastAction.aftermath(results, context, drone);
    }

    /**
     * This method allows to deliver a report of what happened on the island.
     *
     * @return The final report of the exploration.
     */
    @Override
    public String deliverFinalReport() {
        StringBuilder finalReport = new StringBuilder();

        if (drone.getIsBack()) {
            finalReport.append("CREEK: ");
            finalReport.append(landingCreek);
        } else {
            finalReport.append("Creeks found:\n")
                    .append(context.getCreeks());
        }
        return String.valueOf(finalReport);
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the drone.
     *
     * @return the drone which is currently exploring.
     */
    public Drone getDrone() {
        return drone;
    }

    /**
     * Getter of the context.
     *
     * @return the context of the current exploration.
     */
    public Context getContext() {
        return context;
    }

    public Action getLastAction() {
        return lastAction;
    }

}