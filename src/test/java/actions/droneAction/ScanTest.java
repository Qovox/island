package actions.droneAction;

import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Scan;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ScanTest {

    private Drone drone;
    private Context context;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void scanToString() {
        String scanToString = "{\"action\":\"scan\"}";
        String badScanToString = "{\"action\":\"scanne\"}";
        Scan scan = new Scan();
        assertEquals(scanToString, scan.toString());
        assertEquals("scan", scan.getName());
        assertNotEquals(badScanToString, scan.toString());
    }

    @Test
    public void scanAftermath() {
        String result = "{\"cost\":4,\"extras\":{\"biomes\":[\"BEACH\"],\"creeks\":[\"id\"],\"sites\": []}}";
        new Scan().aftermath(result, context, drone);

        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.BEACH);

        Tile expectedTile = new Tile(new Point(0, 0), true, false, "id", null, biomesList);
        int lastIndex = context.getMap().getMap().size() - 1;

        assertEquals(19996, context.getBudget().getBudgetLeft());
        assertEquals(expectedTile.getPosition(), context.getMap().getMap().get(lastIndex).getPosition());
        assertEquals(expectedTile.isGround(), context.getMap().getMap().get(lastIndex).isGround());
        assertEquals(expectedTile.isDanger(), context.getMap().getMap().get(lastIndex).isDanger());
        assertEquals(expectedTile.idCreek(), context.getMap().getMap().get(lastIndex).idCreek());
        assertEquals(expectedTile.idEmergencySite(), context.getMap().getMap().get(lastIndex).idEmergencySite());
        assertEquals(expectedTile.getBiomesList(), context.getMap().getMap().get(lastIndex).getBiomesList());
    }

    @Test
    public void scanAftermath2() {
        String result = "{\"cost\":8,\"extras\":{\"biomes\":[\"OCEAN\", \"BEACH\", \"TROPICAL_RAIN_FOREST\"],\"creeks\":[],\"sites\": [\"id\"]}}";
        new Scan().aftermath(result, context, drone);

        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.OCEAN);
        biomesList.add(Biomes.BEACH);
        biomesList.add(Biomes.TROPICAL_RAIN_FOREST);
        Tile expectedTile = new Tile(new Point(0, 0), true, false, null, "id", biomesList);

        int lastIndex = context.getMap().getMap().size() - 1;

        assertEquals(19992, context.getBudget().getBudgetLeft());
        assertEquals(expectedTile.getPosition(), context.getMap().getMap().get(lastIndex).getPosition());
        assertEquals(expectedTile.isGround(), context.getMap().getMap().get(lastIndex).isGround());
        assertEquals(expectedTile.isDanger(), context.getMap().getMap().get(lastIndex).isDanger());
        assertEquals(expectedTile.idCreek(), context.getMap().getMap().get(lastIndex).idCreek());
        assertEquals(expectedTile.idEmergencySite(), context.getMap().getMap().get(lastIndex).idEmergencySite());
        assertEquals(expectedTile.getBiomesList(), context.getMap().getMap().get(lastIndex).getBiomesList());
    }

}
