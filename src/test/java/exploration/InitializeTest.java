package exploration;

import fr.unice.polytech.si3.qgl.iabc.exploration.Initialize;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InitializeTest {

    private String contract;

    //(WOOD, 12000), (FRUITS, 100), (ORE, 10), (FUR, 200), (RUM, 15)
    @Before
    public void setUp() {
        contract = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":12000,\"resource\":\"WOOD\"}," +
                "{ \"amount\":100,\"resource\":\"FRUITS\"}," +
                "{ \"amount\":10,\"resource\":\"ORE\"}," +
                "{ \"amount\":200,\"resource\":\"FUR\"}," +
                "{ \"amount\":15,\"resource\":\"RUM\"}]," +
                "\"heading\":\"W\"}";
    }

    @After
    public void tearDown() {

    }

    @Test
    public void initialize() {
        Initialize initialize = new Initialize(contract);
        Inventory inventory = initialize.initialize();
        assertEquals(6, inventory.getInventory().size());
        assertEquals(145, (int) inventory.getInventory().get(Resources.FRUITS));
        assertEquals(180, (int) inventory.getInventory().get(Resources.SUGAR_CANE));
    }

}
