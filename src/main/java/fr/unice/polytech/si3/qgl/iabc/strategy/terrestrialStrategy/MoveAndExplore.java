package fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Explore;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.MoveTo;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.strategy.Strategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.0
 *          This part of strategy simply move in a given direction and the
 *          explore on the tile where the group moved to.
 */
public class MoveAndExplore implements Strategy {

    //the direction of the movement.
    private Directions direction;

    /**
     * Constructor of MoveAndExplore.
     *
     * @param direction the moving direction.
     */
    public MoveAndExplore(Directions direction) {
        this.direction = direction;
    }

    @Override
    public Action takeDecision() {
        return null;
    }

    @Override
    public List<Action> takeDecisionList() {
        List<Action> actions = new ArrayList<>();
        actions.add(new Explore());
        actions.add(new MoveTo(direction));
        return actions;
    }

}
