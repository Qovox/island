package fr.unice.polytech.si3.qgl.iabc.map;

import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Maven
 * @version 1.5
 *          This class creates a Map of tile while exploring.
 *          We can access the different tiles of the map.
 */
public class AerialMap extends Map {

    //the list of the creeks.
    private List<Tile> creeksList;
    //the emergency site.
    private Tile emergencySite;

    /**
     * It creates an empty map.
     */
    public AerialMap() {
        creeksList = new ArrayList<>();
    }

    /**
     * This method updates the map after an echo.
     *
     * @param drone            the drone which explores.
     * @param commandDirection the direction in which the echo has been done.
     * @param range            the range found in the results.
     * @param found            a string equals either to OUT_OF_RANGE or GROUND.
     */
    public void mapEcho(Drone drone, Directions commandDirection, int range, String found) {
        Point initPosition = new Point(drone.getPosition());
        Point position = new Point(drone.getPosition());
        List<Biomes> biomesList = new ArrayList<>();
        if ("OUT_OF_RANGE".equals(found)) {
            drone.getRadar().processMaxRange(drone.getPosition(), commandDirection, range);
        }

        int i = 0;
        if (range != 0)
            i = 1;
        for (; i <= range; i++) {
            boolean isGround = false;
            boolean isDanger = false;
            position = new Point(drone.fly(commandDirection, position));

            if ("GROUND".equals(found)) {
                if (range == 0 || range - i == 0) {
                    isGround = true;
                } else {
                    biomesList.add(Biomes.OCEAN);
                }
            } else if (range == 0) {
                biomesList.add(Biomes.OCEAN);
                position = initPosition;
                isDanger = true;
            } else if (range - i < 2) {
                biomesList.add(Biomes.OCEAN);
                isDanger = true;
            } else {
                biomesList.add(Biomes.OCEAN);
            }
            Tile tile = new Tile(position, isGround, isDanger, null, null, biomesList);
            this.addTile(tile);
            biomesList = new ArrayList<>();
        }
    }

    /**
     * This method allows the update of the map after a scan.
     *
     * @param position the coordinates.
     */
    public void mapScan(Point position, List<Biomes> biomesList, List<String> creeksList, String sites) {
        Tile tile = new Tile(position, false, false, null, null, biomesList);

        for (Biomes biomes : biomesList) {
            if (!biomes.equals(Biomes.OCEAN)) {
                tile.setIsGround(true);
                break;
            }
        }
        if (!creeksList.isEmpty()) {
            tile.setIdCreek(creeksList.get(0));
            this.creeksList.add(tile);
        }
        if (!"".equals(sites)) {
            tile.setIdEmergencySite(sites);
            emergencySite = tile;
        }
        this.addTile(tile);
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the creeksList found.
     *
     * @return the list of creeksList found.
     */
    public List<Tile> getCreeksList() {
        return creeksList;
    }

    /**
     * Getter of the emergency site.
     *
     * @return the tile which has an emergency site.
     */
    public Optional<Tile> getEmergencySite() {
        return Optional.ofNullable(emergencySite);
    }

}
