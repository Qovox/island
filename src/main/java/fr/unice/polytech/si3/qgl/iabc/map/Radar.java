package fr.unice.polytech.si3.qgl.iabc.map;

import java.awt.*;
import java.util.Optional;

/**
 * @author Maven
 * @version 1.2
 *          This class represents the radar. It allows to calculates the max range
 *          one the map, in each direction. Also, it can determine in which direction
 *          the max range is, according to the current heading.
 */
public class Radar {

    private Optional<Tile> forward = Optional.empty();
    private Optional<Tile> left = Optional.empty();
    private Optional<Tile> right = Optional.empty();

    //the maximum range in each direction.
    private MaxRange northMaxRange;
    private MaxRange eastMaxRange;
    private MaxRange southMaxRange;
    private MaxRange westMaxRange;

    /**
     * Constructor of the class Radar.
     */
    public Radar() {
        northMaxRange = new MaxRange();
        eastMaxRange = new MaxRange();
        southMaxRange = new MaxRange();
        westMaxRange = new MaxRange();
    }

    /**
     * Reset the surrounding positions.
     */
    public void updateSurroundingTile(Point position, Directions heading, AerialMap aerialMap) {
        Point newForward = new Point(position);
        Point newLeft = new Point(position);
        Point newRight = new Point(position);

        switch (heading) {
            case NORTH:
                newForward.translate(0, 1);
                newLeft.translate(-1, 0);
                newRight.translate(1, 0);
                break;
            case EAST:
                newForward.translate(1, 0);
                newLeft.translate(0, 1);
                newRight.translate(0, -1);
                break;
            case SOUTH:
                newForward.translate(0, -1);
                newLeft.translate(1, 0);
                newRight.translate(-1, 0);
                break;
            case WEST:
                newForward.translate(-1, 0);
                newLeft.translate(0, -1);
                newRight.translate(0, 1);
                break;
            default:
                break;
        }

        this.forward = aerialMap.getTileByPosition(newForward);
        this.left = aerialMap.getTileByPosition(newLeft);
        this.right = aerialMap.getTileByPosition(newRight);
    }

    /**
     * This method initializes the max range in each direction when found.
     */
    public void processMaxRange(Point position, Directions direction, int range) {
        int x = (int) position.getX();
        int y = (int) position.getY();

        switch (direction) {
            case NORTH:
                if (y >= 0) {
                    northMaxRange.build(y + range);
                } else {
                    northMaxRange.build(range - y);
                }
                break;
            case EAST:
                if (x >= 0) {
                    eastMaxRange.build(x + range);
                } else {
                    eastMaxRange.build(range - x);
                }
                break;
            case SOUTH:
                if (y >= 0) {
                    southMaxRange.build(range - y);
                } else {
                    southMaxRange.build(y + range);
                }
                break;
            case WEST:
                if (x >= 0) {
                    westMaxRange.build(range - x);
                } else {
                    westMaxRange.build(x + range);
                }
                break;
            default:
                break;
        }
    }

    /**
     * This method returns the direction between left and right that have more space
     *
     * @param heading  the drone's current heading.
     * @param position the coordinates.
     * @return the max range direction relative to the position.
     */
    public Directions relativeMaxDirection(Directions heading, Point position) {

        switch (heading) {
            case NORTH:
            case SOUTH:
                return nsRelativeMaxDir((int) position.getX());
            case EAST:
            case WEST:
                return ewRelativeMaxDir((int) position.getY());
            default:
                return heading;
        }
    }

    /**
     * This method allows to calculate the distance between the drone and the border (right/left).
     *
     * @param x the horizontal position.
     * @return the direction of the farthest border.
     */
    private Directions nsRelativeMaxDir(int x) {
        if ((westMaxRange.range + x) >= (eastMaxRange.range - x)) {
            return Directions.WEST;
        } else return Directions.EAST;
    }

    /**
     * This method allows to calculate the distance between the drone and the border (up/down).
     *
     * @param y the vertical position.
     * @return the direction of the farthest border.
     */
    private Directions ewRelativeMaxDir(int y) {
        if ((southMaxRange.range + y) >= (northMaxRange.range - y)) {
            return Directions.SOUTH;
        } else return Directions.NORTH;
    }


    //========================
    //   Getters and setters
    //========================

    /**
     * Getter of the forward tile of the drone.
     *
     * @return the forward tile.
     */
    public Optional<Tile> getForward() {
        return forward;
    }

    /**
     * Getter of the left tile of the drone.
     *
     * @return the left tile.
     */
    public Optional<Tile> getLeft() {
        return left;
    }

    /**
     * Getter of the right tile of the drone.
     *
     * @return the right tile.
     */
    public Optional<Tile> getRight() {
        return right;
    }


    /**
     * Inner class allowing to interact with the object Maxrange.
     */
    private class MaxRange {

        private int range;
        private boolean built;

        MaxRange() {
            built = false;
        }

        /**
         * This method allows to modify only once the range.
         *
         * @param range the max range found for this specific MaxRange.
         */
        void build(int range) {
            if (!isBuilt()) {
                this.range = range;
                built = true;
            }
        }

        /**
         * This method allows to know whether it's built or not.
         *
         * @return whether or not it's built.
         */
        boolean isBuilt() {
            return built;
        }

    }

}
