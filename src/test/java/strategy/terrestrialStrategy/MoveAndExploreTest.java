package strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Explore;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.MoveTo;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy.MoveAndExplore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Maven
 *         Created by Thoma on 2/24/2017.
 */
public class MoveAndExploreTest {

    private MoveAndExplore moveAndExplore;

    @Test
    public void NorthTest() {
        moveAndExplore = new MoveAndExplore(Directions.NORTH);
        List<Action> resultActions = moveAndExplore.takeDecisionList();
        Action expectedAction1 = new Explore();
        Action expectedAction2 = new MoveTo(Directions.NORTH);
        Action resultAction1 = resultActions.get(0);
        Action resultAction2 = resultActions.get(1);

        assertEquals(expectedAction1.toString(), resultAction1.toString());
        assertEquals(expectedAction2.toString(), resultAction2.toString());
    }

    @Test
    public void WestTest() {
        moveAndExplore = new MoveAndExplore(Directions.WEST);
        List<Action> resultActions = moveAndExplore.takeDecisionList();
        Action expectedAction1 = new Explore();
        Action expectedAction2 = new MoveTo(Directions.WEST);
        Action resultAction1 = resultActions.get(0);
        Action resultAction2 = resultActions.get(1);

        assertEquals(expectedAction1.toString(), resultAction1.toString());
        assertEquals(expectedAction2.toString(), resultAction2.toString());
    }

    @Test
    public void EastTest() {
        moveAndExplore = new MoveAndExplore(Directions.EAST);
        List<Action> resultActions = moveAndExplore.takeDecisionList();
        Action expectedAction1 = new Explore();
        Action expectedAction2 = new MoveTo(Directions.EAST);
        Action resultAction1 = resultActions.get(0);
        Action resultAction2 = resultActions.get(1);

        assertEquals(expectedAction1.toString(), resultAction1.toString());
        assertEquals(expectedAction2.toString(), resultAction2.toString());
    }

    @Test
    public void SouthTest() {
        moveAndExplore = new MoveAndExplore(Directions.SOUTH);
        List<Action> resultActions = moveAndExplore.takeDecisionList();
        Action expectedAction1 = new Explore();
        Action expectedAction2 = new MoveTo(Directions.SOUTH);
        Action resultAction1 = resultActions.get(0);
        Action resultAction2 = resultActions.get(1);

        assertEquals(expectedAction1.toString(), resultAction1.toString());
        assertEquals(expectedAction2.toString(), resultAction2.toString());
    }
}
