package fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Glimpse;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap;
import fr.unice.polytech.si3.qgl.iabc.strategy.Strategy;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.0
 *          This part of the strategy correctly locates the men to allow them to begin
 *          to explore in good conditions.
 */
public class SearchSafeGround implements Strategy {

    private int glimpseLevel;
    private Point menPosition;
    private TerrestrialMap map;

    /**
     * Constructor SearchSafeGround.
     *
     * @param map         the current map.
     * @param menPosition the current men's position
     */
    public SearchSafeGround(TerrestrialMap map, Point menPosition, int glimpseLevel) {
        this.map = map;
        this.menPosition = menPosition;
        this.glimpseLevel = glimpseLevel;
    }

    @Override
    public Action takeDecision() {
        return null;
    }

    @Override
    public List<Action> takeDecisionList() {
        List<Action> actions = new ArrayList<>();
        for (Directions dir : Directions.values()) {
            if (map.getTileByPositionWithDirection(menPosition, dir, glimpseLevel - 1).isPresent())
                continue;
            actions.add(new Glimpse(dir, glimpseLevel));
        }
        return actions;
    }

}
