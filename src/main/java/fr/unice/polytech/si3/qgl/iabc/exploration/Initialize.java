package fr.unice.polytech.si3.qgl.iabc.exploration;

import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * This class allows to get all information from the context.
 */
public class Initialize {

    //the Json object to switch from a string to a JSON.
    private JSONObject stringToJSON;
    //the men.
    private int men;
    //the given budget.
    private int budget;
    //the given heading at the start of the game.
    private Directions heading;
    //the given contract of the game.
    private Map<Resources, Integer> contract = new EnumMap<>(Resources.class);

    /**
     * Constructor of the class Initialize.
     *
     * @param information the context given when the game starts.
     */
    public Initialize(String information) {
        stringToJSON = new JSONObject(information);
    }


    /**
     * This method processes information in the context.
     */
    public Inventory initialize() {
        men = stringToJSON.getInt("men");
        budget = stringToJSON.getInt("budget");
        heading = Directions.getDirectionName(stringToJSON.getString("heading"));
        JSONArray jsonContracts = stringToJSON.getJSONArray("contracts");
        for (int i = 0; i < jsonContracts.length(); i++) {
            JSONObject contractsObject = jsonContracts.getJSONObject(i);
            Resources resource = Resources.getResourceName(contractsObject.getString("resource"));
            contract.put(resource, contractsObject.getInt("amount"));
        }
        contract = manageManufacturedResources(contract);
        return new Inventory(contract);
    }

    /**
     * This method allows to manage the required manufactured resources by calculating the amount of
     * the needed primary resources.
     *
     * @param inventory the inventory of the men.
     * @return the primary resources needed to make the manufactured resources.
     */
    public Map<Resources, Integer> manageManufacturedResources(Map<Resources, Integer> inventory) {
        Map<Resources, Integer> resourcesToTransform = new HashMap<>();
        List<Resources> resourcesToCollect = new ArrayList<>();
        resourcesToCollect.addAll(inventory.keySet());
        for (Resources resource : resourcesToCollect) {
            if (resource.isManufactured()) {
                resourcesToTransform.putAll(resource.getRecipe());
                for (Resources recipeResource : resourcesToTransform.keySet()) {
                    if (inventory.containsKey(recipeResource))
                        inventory.put(recipeResource, inventory.get(recipeResource) + inventory.get(resource) * resourcesToTransform.get(recipeResource));
                    else
                        inventory.put(recipeResource, inventory.get(resource) * resourcesToTransform.get(recipeResource));
                }
                resourcesToTransform = new HashMap<>();
            }
        }
        return inventory;
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the number of men.
     *
     * @return the number of men to explore the island.
     */
    public int getMen() {
        return men;
    }

    /**
     * Getter of the budget.
     *
     * @return the initial bugdet to complete the contract.
     */
    public int getBudget() {
        return budget;
    }

    /**
     * Getter of the initial drone's heading.
     *
     * @return the direction which is the initial drone's heading.
     */
    public Directions getHeading() {
        return heading;
    }

}
