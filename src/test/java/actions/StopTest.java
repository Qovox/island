package actions;

import fr.unice.polytech.si3.qgl.iabc.actions.Stop;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class StopTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void stopToString() {
        String stopToString = "{\"action\":\"stop\"}";
        String badStopToString = "{\"action\":\"stoppe\"}";
        Stop stop = new Stop();
        assertEquals(stopToString, new Stop().toString());
        assertEquals("stop", stop.getName());
        assertNotEquals(badStopToString, stop);
    }

}
