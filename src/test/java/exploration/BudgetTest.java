package exploration;

import fr.unice.polytech.si3.qgl.iabc.exploration.Budget;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BudgetTest {

    private Budget budget;

    @Before
    public void setUp() {
        budget = new Budget(10000);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void check() {
        budget.spend(2500);
        assertTrue(budget.check());
        budget.spend(7419);
        assertFalse(budget.check());
    }

    @Test
    public void spend() {
        budget.spend(15);
        assertEquals(9985, budget.getBudgetLeft());
        budget.spend(10000);
        assertEquals(-15, budget.getBudgetLeft());
    }

}
