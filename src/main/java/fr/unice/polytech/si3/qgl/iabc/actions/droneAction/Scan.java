package fr.unice.polytech.si3.qgl.iabc.actions.droneAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.2
 *          This class implements the scan action behavior.
 */
public class Scan extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;

    /**
     * {"action":"scan"}
     * Constructor of the class Scan.
     */
    public Scan() {
        name = "scan";
        action = new JSONObject();
        action.put("action", name);
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param drone   the previous drone to update.
     */
    @Override
    public void aftermath(String result, Context context, Drone drone) {
        super.aftermath(result, context, drone);

        JSONObject stringToJSON = new JSONObject(result);
        JSONObject extrasObjects = stringToJSON.getJSONObject("extras");
        JSONArray jsonBiomes = extrasObjects.getJSONArray("biomes");
        JSONArray jsonCreeks = extrasObjects.getJSONArray("creeks");
        JSONArray jsonSites = extrasObjects.getJSONArray("sites");

        List<Biomes> biomesList = new ArrayList<>();
        for (int i = 0; i < jsonBiomes.length(); i++) {
            biomesList.add(Biomes.getBiomeName(jsonBiomes.getString(i)));
        }
        List<String> creeksList = new ArrayList<>();
        if (jsonCreeks.length() != 0) {
            creeksList.add(jsonCreeks.getString(0));
        }
        String sites = "";
        if (jsonSites.length() != 0) {
            sites = String.valueOf(jsonSites.getString(0));
        }

        context.mapScan(drone, biomesList, creeksList, sites);
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

}
