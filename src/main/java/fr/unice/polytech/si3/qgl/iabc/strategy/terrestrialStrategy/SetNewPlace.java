package fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.MoveTo;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap;
import fr.unice.polytech.si3.qgl.iabc.strategy.Strategy;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.0
 *          This part of the strategy moves the group of men in diagonal direction
 *          so that the group can explore more clearly
 */
public class SetNewPlace implements Strategy {

    private Radar radar;
    private boolean inDanger;
    private Point menPosition;
    private TerrestrialMap map;
    private Directions explorationDirection;

    /**
     * Constructor of SetNewPlace.
     */
    public SetNewPlace(TerrestrialMap map, Point menPosition, Radar radar, Directions explorationDirection, boolean inDanger) {
        this.explorationDirection = explorationDirection;
        this.menPosition = menPosition;
        this.inDanger = inDanger;
        this.radar = radar;
        this.map = map;
    }

    @Override
    public Action takeDecision() {
        return null;
    }

    @Override
    public List<Action> takeDecisionList() {
        List<Action> actions = new ArrayList<>();
        int i = 0;
        while (map.getTileByPositionWithDirection(menPosition, explorationDirection, i).isPresent()) {
            actions.add(new MoveTo(explorationDirection));
            i++;
        }
        actions.add(new MoveTo(explorationDirection));
        if (inDanger)
            actions.add(new MoveTo(radar.relativeMaxDirection(explorationDirection, menPosition)));
        return actions;
    }

}
