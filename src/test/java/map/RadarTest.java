package map;

import fr.unice.polytech.si3.qgl.iabc.map.AerialMap;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class RadarTest {

    private fr.unice.polytech.si3.qgl.iabc.map.Radar radar;

    @Before
    public void setUp() {
        radar = new fr.unice.polytech.si3.qgl.iabc.map.Radar();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void updateSurroundingTiles() {
        AerialMap aerialMap = new AerialMap();
        aerialMap.getMap().add(new Tile(new Point(0, 0), false, false, null, null, null));
        //Surrounding tiles
        Tile northTile = new Tile(new Point(0, 1), false, false, null, null, null);
        Tile eastTile = new Tile(new Point(1, 0), false, false, null, null, null);
        Tile southTile = new Tile(new Point(0, -1), false, false, null, null, null);
        Tile westTile = new Tile(new Point(-1, 0), false, false, null, null, null);
        aerialMap.getMap().add(northTile);
        aerialMap.getMap().add(eastTile);
        aerialMap.getMap().add(southTile);
        aerialMap.getMap().add(westTile);

        radar.updateSurroundingTile(new Point(0, 0), Directions.NORTH, aerialMap);
        assertEquals(Optional.of(northTile), radar.getForward());
        assertEquals(Optional.of(westTile), radar.getLeft());
        assertEquals(Optional.of(eastTile), radar.getRight());

        radar.updateSurroundingTile(new Point(0, 0), Directions.SOUTH, aerialMap);
        assertEquals(Optional.of(southTile), radar.getForward());
        assertEquals(Optional.of(eastTile), radar.getLeft());
        assertEquals(Optional.of(westTile), radar.getRight());

    }

    @Test
    public void relativeMaxDirection() {
        radar.processMaxRange(new Point(35, -2), Directions.NORTH, 12);
        radar.processMaxRange(new Point(35, -2), Directions.EAST, 17);
        radar.processMaxRange(new Point(35, -2), Directions.SOUTH, 40);
        radar.processMaxRange(new Point(35, -2), Directions.WEST, 35);
        assertEquals(Directions.NORTH, radar.relativeMaxDirection(Directions.WEST, new Point(42, -28)));
        assertEquals(Directions.WEST, radar.relativeMaxDirection(Directions.SOUTH, new Point(42, -28)));
        assertEquals(Directions.SOUTH, radar.relativeMaxDirection(Directions.EAST, new Point(8, 5)));
        assertEquals(Directions.EAST, radar.relativeMaxDirection(Directions.NORTH, new Point(8, 5)));
    }

    @Test
    public void forbiddenRelativeMaxDirection() {
        radar.processMaxRange(new Point(0, 0), Directions.NORTH, 12);
        radar.processMaxRange(new Point(0, 0), Directions.EAST, 4);
        radar.processMaxRange(new Point(0, 0), Directions.SOUTH, 40);
        radar.processMaxRange(new Point(0, 0), Directions.WEST, 25);

        assertNotEquals(Directions.NORTH, radar.relativeMaxDirection(Directions.NORTH, new Point(2, 2)));
        assertNotEquals(Directions.SOUTH, radar.relativeMaxDirection(Directions.NORTH, new Point(2, 2)));

        assertNotEquals(Directions.EAST, radar.relativeMaxDirection(Directions.EAST, new Point(2, 2)));
        assertNotEquals(Directions.WEST, radar.relativeMaxDirection(Directions.EAST, new Point(2, 2)));

        assertNotEquals(Directions.WEST, radar.relativeMaxDirection(Directions.WEST, new Point(2, 2)));
        assertNotEquals(Directions.EAST, radar.relativeMaxDirection(Directions.WEST, new Point(2, 2)));

        assertNotEquals(Directions.NORTH, radar.relativeMaxDirection(Directions.SOUTH, new Point(2, 2)));
        assertNotEquals(Directions.SOUTH, radar.relativeMaxDirection(Directions.SOUTH, new Point(2, 2)));
    }

}
