package actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Explore;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ExploreTest {

    private Men men;
    private Context context;
    private Drone drone;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 10,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":6000,\"resource\":\"WOOD\"}," +
                "{ \"amount\":2000,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        men = new Men(new Point(), new Inventory(new HashMap<>()), new Radar());
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void exploreToString() {
        Explore explore = new Explore();
        String ExploreToString = "{\"action\":\"explore\"}";
        String BadExploreToString = "{\"action\":\"explores\"}";
        assertEquals(ExploreToString, explore.toString());
        assertNotEquals(BadExploreToString, explore.toString());
        assertEquals("explore", explore.getName());
    }


    @Test
    public void exploreAftermath() {
        String result = "{\n" +
                "  \"cost\": 9,\n" +
                "  \"extras\": {\n" +
                "    \"resources\": [\n" +
                "      { \"amount\": \"HIGH\", \"resource\": \"FUR\", \"cond\": \"FAIR\" },\n" +
                "      { \"amount\": \"LOW\", \"resource\": \"WOOD\", \"cond\": \"HARSH\" }\n" +
                "    ],\n" +
                "    \"pois\": [{\"kind\": \"Creek\", \"id\": \"43e3eb42-50f0-47c5-afa3-16cd3d50faff\"}]\n" +
                "  },\n" +
                "  \"status\": \"OK\"\n" +
                "}";
        new Explore().aftermath(result, context, men);
        assertEquals(19991, context.getBudget().getBudgetLeft());
    }
}
