package fr.unice.polytech.si3.qgl.iabc.actions.droneAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import org.json.JSONObject;

/**
 * @author Maven
 * @version 1.2
 *          This class implements the heading action behavior.
 */
public class Heading extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;
    private Directions direction;

    /**
     * {"action":"heading","parameters":{"direction":"N"}}
     * Constructor of the class Heading.
     *
     * @param direction the direction.
     */
    public Heading(Directions direction) {
        name = "heading";
        action = new JSONObject();
        action.put("action", name);
        action.put("parameters", new JSONObject().put("direction", direction));
        this.direction = direction;
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param drone   the previous drone to update.
     */
    @Override
    public void aftermath(String result, Context context, Drone drone) {
        super.aftermath(result, context, drone);
        context.headingUpdate(drone, direction);
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

}
