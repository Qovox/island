package fr.unice.polytech.si3.qgl.iabc.actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Amount;
import fr.unice.polytech.si3.qgl.iabc.map.Conditions;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.2
 *          This class implements the explore action behavior.
 */
public class Explore extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;

    /**
     * {"action":"explore"}
     * Constructor of the class Explore.
     */
    public Explore() {
        name = "explore";
        action = new JSONObject();
        action.put("action", name);
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param men     the previous men to update.
     */
    @Override
    public void aftermath(String result, Context context, Men men) {
        super.aftermath(result, context, men);
        JSONObject stringToJSON = new JSONObject(result);
        JSONObject extrasObjects = stringToJSON.getJSONObject("extras");
        JSONArray jsonResources = extrasObjects.getJSONArray("resources");
        List<Amount> amount = new ArrayList<>();
        List<Resources> resources = new ArrayList<>();
        List<Conditions> cond = new ArrayList<>();
        for (int i = 0; i < jsonResources.length(); i++) {
            JSONObject exploreResults = (JSONObject) jsonResources.get(i);
            amount.add(Amount.getAmountName(exploreResults.getString("amount")));
            resources.add(Resources.getResourceName(exploreResults.getString("resource")));
            cond.add(Conditions.getConditionName(exploreResults.getString("cond")));
        }
        men.exploreUpdate(amount, resources, cond);
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

}
