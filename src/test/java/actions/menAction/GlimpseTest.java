package actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Glimpse;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.MoveTo;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GlimpseTest {

    private Men men;
    private Context context;
    private Drone drone;

    @Before
    public void setUp() {        String stringContext = "{\"men\": 10,\"budget\":20000,\"contracts\":" +
            "[{ \"amount\":6000,\"resource\":\"WOOD\"}," +
            "{ \"amount\":2000,\"resource\":\"GLASS\"}]," +
            "\"heading\":\"W\"}";
        men = new Men(new Point(), new Inventory(new HashMap<>()), new Radar());
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void glimpseToString() {
        Glimpse glimpse = new Glimpse(Directions.WEST, 2);
        JSONObject parameters = new JSONObject();
        parameters.put("direction", Directions.WEST);
        parameters.put("range", 2);
        JSONObject action = new JSONObject();
        action.put("action", "glimpse");
        action.put("parameters", parameters);
        String badGlimpseToString = "{\"action\":\"glympse\",\"parameters\":{\"direction\":\"N\",\"range\":2}}}";
        assertEquals("glimpse", glimpse.getName());
        assertEquals(action.toString(), glimpse.toString());
        assertNotEquals(badGlimpseToString, glimpse.toString());
        assertEquals(2, glimpse.getRange());
    }

    @Test
    public void jsonParserTest(){
        String acknowledgeResult = "{ \n" +
                "  \"cost\": 3,\n" +
                "  \"extras\": {\n" +
                "    \"asked_range\": 4,\n" +
                "    \"report\": [\n" +
                "      [ [ \"SHRUBLAND\", 59.35 ], [ \"OCEAN\", 40.65 ] ],\n" +
                "      [ [ \"OCEAN\", 70.2  ], [ \"BEACH\", 29.8  ] ],\n" +
                "      [ \"OCEAN\", \"BEACH\" ],\n" +
                "      [ \"OCEAN\" ]\n" +
                "    ]\n" +
                "  },\n" +
                "  \"status\": \"OK\"\n" +
                "}  ";

        List<List<Biomes>> result = new Glimpse(Directions.NORTH, 4).jsonParser(acknowledgeResult);
        assertEquals(4, result.size());

        assertEquals(2, result.get(0).size());
        assertEquals(2, result.get(1).size());
        assertEquals(2, result.get(2).size());
        assertEquals(1, result.get(3).size());

        assertEquals(Biomes.SHRUBLAND, result.get(0).get(0));
        assertEquals(Biomes.OCEAN, result.get(0).get(1));

        assertEquals(Biomes.OCEAN, result.get(1).get(0));
        assertEquals(Biomes.BEACH, result.get(1).get(1));

        assertEquals(Biomes.OCEAN, result.get(2).get(0));
        assertEquals(Biomes.BEACH, result.get(2).get(1));

        assertEquals(Biomes.OCEAN, result.get(3).get(0));
    }


    @Test
    public void glimpseAftermath(){
        String result = "{ \n" +
                "  \"cost\": 12,\n" +
                "  \"extras\": {\n" +
                "    \"asked_range\": 4,\n" +
                "    \"report\": [\n" +
                "      [ [ \"BEACH\", 59.35 ], [ \"OCEAN\", 40.65 ] ],\n" +
                "      [ [ \"OCEAN\", 70.2  ], [ \"BEACH\", 29.8  ] ],\n" +
                "      [ \"OCEAN\", \"BEACH\" ],\n" +
                "      [ \"OCEAN\" ]\n" +
                "    ]\n" +
                "  },\n" +
                "  \"status\": \"OK\"\n" +
                "}";
        new Glimpse(Directions.NORTH, 4).aftermath(result, context, men);
        assertEquals(19988, context.getBudget().getBudgetLeft());






    }

}
