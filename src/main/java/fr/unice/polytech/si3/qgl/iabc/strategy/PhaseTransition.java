package fr.unice.polytech.si3.qgl.iabc.strategy;

import fr.unice.polytech.si3.qgl.iabc.map.AerialMap;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;

import java.awt.*;
import java.util.List;
import java.util.Optional;

/**
 * @author Maven
 * @version 1.2
 *          The role of the this class is to carry out a safe phase transition.
 *          It has to send the land() command to the Explorer takeDecision() method, thus it also has to calculate
 *          among the discovered creeks the closest one to the emergency site.
 */
public class PhaseTransition {

    /**
     * This method allows to initialize the ground mission starting at the closest creek from the emergency site.
     *
     * @return the string of the landing tile.
     */
    public String landingTile(AerialMap aerialMap) {
        List<Tile> creeksList = aerialMap.getCreeksList();
        Optional<Tile> emergencySite = aerialMap.getEmergencySite();
        Tile closestTile = creeksList.get(0);

        if (emergencySite.isPresent()) {
            double closestDistance = distance(closestTile.getPosition(), emergencySite.get().getPosition());
            for (int i = 1; i < creeksList.size(); i++) {
                double distance = distance(creeksList.get(i).getPosition(), emergencySite.get().getPosition());
                if (distance < closestDistance) {
                    closestDistance = distance;
                    closestTile = creeksList.get(i);
                }
            }
            return closestTile.idCreek();
        } else return "";
    }

    /**
     * This method calculates the distance between two points.
     *
     * @param p1 the coordinates of the first point.
     * @param p2 the coordinates of the second point.
     * @return the euclidean distance between the point p1 and p2.
     */
    public double distance(Point p1, Point p2) {
        return p1.distance(p2);
    }

}
