package actions.droneAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Fly;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FlyTest {

    private Drone drone;
    private Context context;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void flyToString() {
        String flyToString = "{\"action\":\"fly\"}";
        String badFlyToString = "{\"action\":\"flie\"}";
        assertEquals(flyToString, new Fly().toString());
        assertEquals("fly", new Fly().getName());
        assertNotEquals(badFlyToString, new Fly().toString());
    }

    @Test
    public void flyAftermath() {
        String result = "{\"cost\":22}";
        Action fly = new Fly();

        fly.aftermath(result, context, drone);
        assertEquals(19978, context.getBudget().getBudgetLeft());
        assertEquals(new Point(-1, 0), drone.getPosition());

        drone.setHeading(Directions.SOUTH);

        fly.aftermath(result, context, drone);
        assertEquals(19956, context.getBudget().getBudgetLeft());
        assertEquals(new Point(-1, -1), drone.getPosition());
    }

}
