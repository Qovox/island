package fr.unice.polytech.si3.qgl.iabc.actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import org.json.JSONObject;

/**
 * @author Maven
 * @version 1.2
 *          This class implements the moveTo action behavior.
 */
public class MoveTo extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;
    //the direction of the action.
    private Directions direction;

    /**
     * {"action":"move_to","parameters":{"direction":"N"}}
     * Constructor of the class MoveTo.
     *
     * @param direction the direction to move to.
     */
    public MoveTo(Directions direction) {
        name = "move_to";
        action = new JSONObject();
        action.put("action", name);
        action.put("parameters", new JSONObject().put("direction", direction));
        this.direction = direction;
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param men     the previous drone to update.
     */
    @Override
    public void aftermath(String result, Context context, Men men) {
        super.aftermath(result, context, men);
        men.moveUpdate(direction);
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Getter of the direction.
     *
     * @return the direction used to do this action.
     */
    public Directions getDirection() {
        return direction;
    }

}
