package strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Scout;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap;
import fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy.ScoutAround;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ScoutAroundTest {

    private ScoutAround scoutAround;
    private TerrestrialMap map = new TerrestrialMap();

    @Test
    public void scoutInAllDirections() {
        scoutAround = new ScoutAround(map, new Point());
        List<Action> resultActions = new ArrayList<>();
        resultActions.add(new Scout(Directions.NORTH));
        resultActions.add(new Scout(Directions.SOUTH));
        resultActions.add(new Scout(Directions.EAST));
        resultActions.add(new Scout(Directions.WEST));
        assertEquals(resultActions.toString(), scoutAround.takeDecisionList().toString());
    }

}