package fr.unice.polytech.si3.qgl.iabc.actions;

import org.json.JSONObject;

/**
 * @author Maven
 * @version 1.0
 *          This class implements the stop action behavior.
 */
public class Stop extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;

    /**
     * {"action":"stop"}
     * Constructor of the class Stop.
     */
    public Stop() {
        name = "stop";
        action = new JSONObject();
        action.put("action", name);
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

}
