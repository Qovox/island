package actions;

import fr.unice.polytech.si3.qgl.iabc.actions.Land;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LandTest {

    private Drone drone;
    private Context context;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void landToString() {
        String landToString = "{\"action\":\"land\",\"parameters\":{" +
                "\"creek\":\"9d63bf93-484a-489a-9ac6-e81377734ef5\"," +
                "\"people\":45}}";
        String badLandToString1 = "{\"action\":\"land\",\"parameters\":{" +
                "\"creek\":\"RandomID\"," +
                "\"people\":45}}";
        String badLandToString2 = "{\"action\":\"land\",\"parameters\":{" +
                "\"creek\":\"9d63bf93-484a-489a-9ac6-e81377734ef5\"," +
                "\"people\":12}}";
        Land land = new Land("9d63bf93-484a-489a-9ac6-e81377734ef5", 45);
        assertEquals(landToString, land.toString());
        assertEquals("land", land.getName());
        assertNotEquals(badLandToString1, land.toString());
        assertNotEquals(badLandToString2, land.toString());
    }

}
