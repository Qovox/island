package fr.unice.polytech.si3.qgl.iabc.map;

/**
 * @author Maven
 * @version 1.0
 *          An enumeration of all available biomes.
 */
public enum Biomes {

    OCEAN, LAKE, BEACH, GRASSLAND,

    MANGROVE, TROPICAL_RAIN_FOREST, TROPICAL_SEASONAL_FOREST,

    TEMPERATE_DECIDUOUS_FOREST, TEMPERATE_RAIN_FOREST, TEMPERATE_DESERT,

    TAIGA, SNOW, TUNDRA, ALPINE, GLACIER,

    SHRUBLAND, SUB_TROPICAL_DESERT;

    /**
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return this.name();
    }

    /**
     * This method allows to get the name of the biome.
     *
     * @param biome the biome to analyze.
     * @return the name of the pointed biome.
     */
    public static Biomes getBiomeName(String biome) {
        for (Biomes biomes : Biomes.values()) {
            if (biomes.toString().equals(biome)) {
                return biomes;
            }
        }
        return null;
    }

}
