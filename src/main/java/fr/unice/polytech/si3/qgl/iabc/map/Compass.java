package fr.unice.polytech.si3.qgl.iabc.map;

import static fr.unice.polytech.si3.qgl.iabc.map.Directions.*;

/**
 * @author Maven
 * @version 1.0
 *          A compass to get the bearings.
 */
public class Compass {

    /**
     * Turn to the right depending on the current direction.
     *
     * @return the correct heading when turning left.
     */
    public Directions left(Directions heading) {
        switch (heading) {
            case NORTH:
                return WEST;
            case WEST:
                return SOUTH;
            case SOUTH:
                return EAST;
            case EAST:
                return NORTH;
            default:
                return null;
        }
    }

    /**
     * Turn to the right depending on the current direction.
     *
     * @return the correct heading when turning right.
     */
    public Directions right(Directions heading) {
        switch (heading) {
            case NORTH:
                return EAST;
            case EAST:
                return SOUTH;
            case SOUTH:
                return WEST;
            case WEST:
                return NORTH;
            default:
                return null;
        }
    }

}
