package strategy;

import fr.unice.polytech.si3.qgl.iabc.strategy.PhaseTransition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;

public class PhaseTransitionTest {

    private PhaseTransition phaseTransition;

    @Before
    public void setUp() {
        phaseTransition = new PhaseTransition();
    }

    @After
    public void tearDown() {

    }

    /**
     * Test the method when points only differs by their abscissas
     */
    @Test
    public void distance_test_x() {
        double distance = phaseTransition.distance(new Point(2, 0), new Point(4, 0));
        assertEquals(distance, 2.0, 0.001);
    }

    /**
     * Test the method when points only differs by their ordinates
     */
    @Test
    public void distance_test_y() {
        double distance = phaseTransition.distance(new Point(0, 4), new Point(0, 2));
        assertEquals(distance, 2.0, 0.001);
    }

    /**
     * Test the method when two points are in a diagonal
     */
    @Test
    public void distance_test_dia() {
        double distance = phaseTransition.distance(new Point(1, 1), new Point(2, 2));
        assertEquals(distance, 1.414, 0.001);
    }

    /**
     * Test the method in a general case
     */
    @Test
    public void distance_test() {
        double distance = phaseTransition.distance(new Point(2, -1), new Point(-2, 2));
        assertEquals(distance, 5.0, 0.001);
    }

}
