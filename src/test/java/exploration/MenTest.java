package exploration;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.Stop;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Scout;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Transform;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MenTest {

    private Inventory inventory;
    private Map<Resources, Integer> contract;

    @Before
    public void setUp() {
        contract = new HashMap<>();
        contract.put(Resources.WOOD, 50);
        contract.put(Resources.QUARTZ, 50);
        contract.put(Resources.GLASS, 2);
        inventory = new Inventory(contract);
    }

    @Test
    public void moveUpdateInEachDirections() {
        Men men = new Men(new Point(), new Inventory(new HashMap<>()), new Radar());
        men.moveUpdate(Directions.NORTH);
        assertEquals(new Point(0, 1), men.getPosition());
        men.moveUpdate(Directions.EAST);
        assertEquals(new Point(1, 1), men.getPosition());
        men.moveUpdate(Directions.SOUTH);
        assertEquals(new Point(1, 0), men.getPosition());
        men.moveUpdate(Directions.WEST);
        assertEquals(new Point(0, 0), men.getPosition());
    }

    @Test
    public void takeDecisionStop() {
        Men men = new Men(new Point(), new Inventory(new HashMap<>()), new Radar());
        assertEquals(new Stop().toString(), men.takeDecision().toString());
    }

    @Test
    public void takeDecisionFirst() {
        Men men = new Men(new Point(), inventory, new Radar());

        assertEquals(new Scout(Directions.WEST).toString(), men.takeDecision().toString());
        assertEquals(new Scout(Directions.EAST).toString(), men.takeDecision().toString());
        assertEquals(new Scout(Directions.SOUTH).toString(), men.takeDecision().toString());
        assertEquals(new Scout(Directions.NORTH).toString(), men.takeDecision().toString());
    }

    @Test
    public void takeDecisionTransform() {
        Men men = new Men(new Point(), inventory, new Radar());

        men.getInventory().update(Resources.WOOD, 40);
        men.getInventory().update(Resources.QUARTZ, 40);
        Map<Resources, Integer> expectedResource = new HashMap<>();
        expectedResource.put(Resources.WOOD, 7);
        expectedResource.put(Resources.QUARTZ, 12);

        assertEquals(new Transform(expectedResource).toString(), men.takeDecision().toString());
    }

}
