package exploration;

import fr.unice.polytech.si3.qgl.iabc.actions.Land;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Scan;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ContextTest {

    private Context context;
    private Drone drone;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void transitionTestAndNumberOfMenAndCreek() {
        java.util.List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.BEACH);
        biomesList.add(Biomes.OCEAN);
        biomesList.add(Biomes.MANGROVE);
        String result1 = "{\"cost\":4,\"extras\":{\"biomes\":[\"BEACH\",\"TAIGA\"]," +
                "\"creeks\":[],\"sites\": [\"9d63bf93-484a-489a-9ac6-e81377734ef5\"]}}";
        new Scan().aftermath(result1, context, drone);
        Tile creek1 = new Tile(new Point(-19, -15), true, false,
                "9d63bf93-484a-489a-9ac6-e81377734ef5", null, biomesList);
        Tile creek2 = new Tile(new Point(3, -21), true, false,
                "7g10vb64-922b-clc2-a209-i48168955uf8", null, biomesList);
        context.getMap().addTile(creek1);
        context.getMap().getCreeksList().add(creek1);
        context.getMap().addTile(creek2);
        context.getMap().getCreeksList().add(creek2);
        Land land = (Land) context.transition();
        assertEquals("7g10vb64-922b-clc2-a209-i48168955uf8", land.getCreek());
        assertEquals(7, land.getPeople());
    }

    @Test
    public void transitionTestNumberOfMenToUse2() {
        String stringContext2 = "{\"men\": 4,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        drone = new Drone();
        context = new Context(stringContext2, drone);
        java.util.List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.BEACH);
        biomesList.add(Biomes.OCEAN);
        biomesList.add(Biomes.MANGROVE);
        String result1 = "{\"cost\":4,\"extras\":{\"biomes\":[\"BEACH\",\"TAIGA\"]," +
                "\"creeks\":[],\"sites\": [\"9d63bf93-484a-489a-9ac6-e81377734ef5\"]}}";
        new Scan().aftermath(result1, context, drone);
        Tile creek1 = new Tile(new Point(-19, -15), true, false,
                "9d63bf93-484a-489a-9ac6-e81377734ef5", null, biomesList);
        context.getMap().addTile(creek1);
        context.getMap().getCreeksList().add(creek1);
        Land land = (Land) context.transition();
        assertEquals(3, land.getPeople());

    }

    @Test
    public void checkSpend() {
        context.spend(9990);
        assertTrue(context.check());
        context.spend(10000);
        assertFalse(context.check());
    }

    @Test
    public void getCreeks() {
        assertEquals("", context.getCreeks());
        context.getMap().getCreeksList().add(new Tile(new Point(12, 25), true, false, "ea45p225-rj7q-t5s1-99g7-1j5oq6t5f9r1", null, null));
        context.getMap().getCreeksList().add(new Tile(new Point(12, 25), true, false, "6ecda41e-5ed5-4706-b243-4548e4191e9d", null, null));
        context.getMap().getCreeksList().add(new Tile(new Point(12, 25), true, false, "a34b19a5-7305-420c-a3b6-150d3cb8953a", null, null));
        String expected =
                "CREEK:ea45p225-rj7q-t5s1-99g7-1j5oq6t5f9r1\n" +
                        "CREEK:6ecda41e-5ed5-4706-b243-4548e4191e9d\n" +
                        "CREEK:a34b19a5-7305-420c-a3b6-150d3cb8953a\n";
        assertEquals(expected, context.getCreeks());
    }

    @Test
    public void getEmergency() {
        assertEquals("", context.getEmergency());
        context.getMap().mapScan(new Point(12, 25), new ArrayList<>(), new ArrayList<>(), "15e0d07b-f370-4c1f-9318-2dba00a5733b");
        String expected = "EMERGENCY:15e0d07b-f370-4c1f-9318-2dba00a5733b";
        assertEquals(expected, context.getEmergency());
    }

}
