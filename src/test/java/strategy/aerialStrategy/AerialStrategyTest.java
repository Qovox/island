package strategy.aerialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Echo;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Fly;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Heading;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Scan;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.AerialMap;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;
import fr.unice.polytech.si3.qgl.iabc.strategy.Strategy;
import fr.unice.polytech.si3.qgl.iabc.strategy.aerialStrategy.AerialStrategy;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AerialStrategyTest {

    private Drone drone;
    private AerialMap aerialMap;
    private AerialStrategy aerialStrategy;

    @Before
    public void setUp() {
        drone = new Drone();
        aerialStrategy = new AerialStrategy(drone);
        drone.setHeading(Directions.WEST);
        aerialMap = new AerialMap();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void implementsInterface() {
        assertTrue(aerialStrategy instanceof Strategy);
    }

    @Test
    public void unknownTest() {
        assertTrue(aerialStrategy.unknown());
    }

    @Test
    public void unknown() {
        assertEquals(new Point(0, 0), drone.getPosition());
        assertTrue(aerialStrategy.unknown());
        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.OCEAN);
        aerialMap.addTile(new Tile(new Point(-1, 0), false, false, biomesList));
        aerialMap.addTile(new Tile(new Point(0, -1), false, false, biomesList));
        aerialStrategy.updateOnMap(aerialMap, null, "", "", -1);
        Assert.assertFalse(aerialStrategy.unknown());
        aerialStrategy.flyUpdate(drone);
        aerialStrategy.updateOnMap(aerialMap, null, "", "", -1);
        assertTrue(aerialStrategy.unknown());
        aerialMap.addTile(new Tile(new Point(-1, -1), false, false, biomesList));
        aerialStrategy.updateOnMap(aerialMap, null, "", "", -1);
        Assert.assertFalse(aerialStrategy.unknown());
    }

    @Test
    public void solveUnknown() {
        assertEquals(new Echo(Directions.WEST).toString(), aerialStrategy.solveUnknown().toString());
        assertEquals(new Echo(Directions.NORTH).toString(), aerialStrategy.takeDecision().toString());
        assertEquals(new Echo(Directions.SOUTH).toString(), aerialStrategy.takeDecision().toString());

        aerialStrategy.headingUpdate(drone, Directions.NORTH);
        assertEquals(new Echo(Directions.NORTH).toString(), aerialStrategy.takeDecision().toString());
        assertEquals(new Echo(Directions.EAST).toString(), aerialStrategy.takeDecision().toString());
        assertEquals(new Echo(Directions.WEST).toString(), aerialStrategy.takeDecision().toString());
    }

    @Test
    public void searchGround() {
        assertEquals(new Fly().toString(), aerialStrategy.searchGround().toString());
        //When no max range has been found, arbitrary, the drone asks an echo in the south direction
        //if the current heading is either WEST or EAST
        assertEquals(new Echo(Directions.SOUTH).toString(), aerialStrategy.takeDecision().toString());

        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.OCEAN);
        aerialMap.addTile(new Tile(new Point(0, 1), false, false, biomesList));
        aerialMap.addTile(new Tile(new Point(-1, 0), false, false, biomesList));
        aerialMap.addTile(new Tile(new Point(1, 0), false, false, biomesList));
        aerialStrategy.headingUpdate(drone, Directions.NORTH);
        aerialStrategy.updateOnMap(aerialMap, null, "", "", -1);
        assertEquals(new Fly().toString(), aerialStrategy.takeDecision().toString());
        //When no max range has been found, arbitrary, the drone asks an echo in the west direction
        //if the current heading is either SOUTH or NORTH
        assertEquals(new Echo(Directions.EAST).toString(), aerialStrategy.takeDecision().toString());
    }

    @Test
    public void scanMap() throws Exception {
        // Testing the first if
        assertEquals(new Scan().toString(), aerialStrategy.scanMap().toString());
        // Testing the second if
        aerialMap.addTile(new Tile(new Point(0, 1), true, false, new ArrayList<>()));
        drone.setHeading(Directions.SOUTH);
        aerialStrategy.updateOnMap(aerialMap, Directions.SOUTH, "echo", "GROUND", 0);
        assertEquals(new Fly().toString(), aerialStrategy.scanMap().toString());
    }

    @Test
    public void particularScanMap() {
        drone.setHeading(Directions.EAST);
        aerialMap.addTile(new Tile(new Point(0, 0), false, false, new ArrayList<>()));
        aerialStrategy.updateOnMap(aerialMap, Directions.EAST, "echo", "OUT_OF_RANGE", 2);
        assertEquals(new Echo(Directions.EAST).toString(), aerialStrategy.particularScanMap().toString());
        aerialMap.addTile(new Tile(new Point(1, 0), true, false, new ArrayList<>()));
        aerialStrategy.flyUpdate(drone);
        aerialStrategy.updateOnMap(aerialMap, Directions.EAST, "echo", "OUT_OF_RANGE", 4);
        aerialStrategy.updateOnMap(aerialMap, Directions.EAST, "echo", "OUT_OF_RANGE", 4);
        aerialStrategy.scanMap();
        assertEquals(new Fly().toString(), aerialStrategy.particularScanMap().toString());
        aerialStrategy.updateOnMap(aerialMap, Directions.EAST, "echo", "OUT_OF_RANGE", 2);
        assertEquals(new Heading(Directions.SOUTH).toString(), aerialStrategy.particularScanMap().toString());
    }

    @Test
    public void turnAround() {
        aerialMap.addTile(new Tile(new Point(0, 0), false, false, new ArrayList<>()));
        aerialStrategy.updateOnMap(aerialMap, Directions.NORTH, "echo", "OUT_OF_RANGE", 4);
    }

    @Test
    public void turnAroundNorth() {
        drone.setHeading(Directions.EAST);
        assertEquals(new Heading(Directions.NORTH).toString(), aerialStrategy.turnAroundNorth().toString());
        drone.setHeading(Directions.WEST);
        assertEquals(new Heading(Directions.NORTH).toString(), aerialStrategy.turnAroundNorth().toString());
        drone.setHeading(Directions.NORTH);
        assertEquals(new Scan().toString(), aerialStrategy.turnAroundNorth().toString());
    }

    @Test
    public void turnAroundEast() {
        drone.setHeading(Directions.NORTH);
        assertEquals(new Heading(Directions.EAST).toString(), aerialStrategy.turnAroundEast().toString());
        drone.setHeading(Directions.SOUTH);
        assertEquals(new Heading(Directions.EAST).toString(), aerialStrategy.turnAroundEast().toString());
        drone.setHeading(Directions.EAST);
        assertEquals(new Scan().toString(), aerialStrategy.turnAroundEast().toString());
    }

    @Test
    public void turnAroundSouth() {
        drone.setHeading(Directions.EAST);
        assertEquals(new Heading(Directions.SOUTH).toString(), aerialStrategy.turnAroundSouth().toString());
        drone.setHeading(Directions.WEST);
        assertEquals(new Heading(Directions.SOUTH).toString(), aerialStrategy.turnAroundSouth().toString());
        drone.setHeading(Directions.SOUTH);
        assertEquals(new Scan().toString(), aerialStrategy.turnAroundSouth().toString());
    }

    @Test
    public void turnAroundWest() {
        drone.setHeading(Directions.NORTH);
        assertEquals(new Heading(Directions.WEST).toString(), aerialStrategy.turnAroundWest().toString());
        drone.setHeading(Directions.SOUTH);
        assertEquals(new Heading(Directions.WEST).toString(), aerialStrategy.turnAroundWest().toString());
        drone.setHeading(Directions.WEST);
        assertEquals(new Scan().toString(), aerialStrategy.turnAroundWest().toString());
    }

    @Test
    public void exploreMorePositioning() {
        drone.setHeading(Directions.SOUTH);
        aerialMap.addTile(new Tile(new Point(0, 0), false, false, new ArrayList<>()));
        aerialStrategy.updateOnMap(aerialMap, Directions.SOUTH, "echo", "OUT_OF_RANGE", 2);
        assertEquals(new Heading(Directions.WEST).toString(), aerialStrategy.exploreMorePositioning().toString());
    }

    @Test
    public void goToTheGround() {
        aerialMap.mapEcho(drone, Directions.NORTH, 12, "GROUND");
        aerialStrategy.updateOnMap(aerialMap, Directions.NORTH, "echo", "GROUND", 12);
        assertEquals(new Heading(Directions.NORTH).toString(), aerialStrategy.takeDecision().toString());
        assertEquals(new Echo(Directions.NORTH).toString(), aerialStrategy.takeDecision().toString());
    }

}
