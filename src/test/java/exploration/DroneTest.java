package exploration;

import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;

public class DroneTest {

    private Drone drone;

    @Before
    public void setUp() {
        drone = new Drone();
        drone.setHeading(Directions.WEST);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void left() {
        assertEquals(Directions.NORTH, drone.left(Directions.EAST));
        assertEquals(Directions.EAST, drone.left(Directions.SOUTH));
        assertEquals(Directions.SOUTH, drone.left(Directions.WEST));
        assertEquals(Directions.WEST, drone.left(Directions.NORTH));
    }

    @Test
    public void right() {
        assertEquals(Directions.NORTH, drone.right(Directions.WEST));
        assertEquals(Directions.EAST, drone.right(Directions.NORTH));
        assertEquals(Directions.SOUTH, drone.right(Directions.EAST));
        assertEquals(Directions.WEST, drone.right(Directions.SOUTH));
    }

    @Test
    public void fly() {
        //Tests are done to ensure negative and positive coordinates update
        assertEquals(new Point(-23, 85), drone.fly(Directions.NORTH, new Point(-23, 84)));
        assertEquals(new Point(-22, 84), drone.fly(Directions.EAST, new Point(-23, 84)));
        assertEquals(new Point(-23, 83), drone.fly(Directions.SOUTH, new Point(-23, 84)));
        assertEquals(new Point(-24, 84), drone.fly(Directions.WEST, new Point(-23, 84)));

        assertEquals(new Point(23, -83), drone.fly(Directions.NORTH, new Point(23, -84)));
        assertEquals(new Point(24, -84), drone.fly(Directions.EAST, new Point(23, -84)));
        assertEquals(new Point(23, -85), drone.fly(Directions.SOUTH, new Point(23, -84)));
        assertEquals(new Point(22, -84), drone.fly(Directions.WEST, new Point(23, -84)));
    }

    @Test
    public void headingPositive() {
        //Heading to the north
        assertEquals(new Point(-22, 85), drone.heading(Directions.NORTH, Directions.EAST, new Point(-23, 84)));
        assertEquals(new Point(-24, 85), drone.heading(Directions.NORTH, Directions.WEST, new Point(-23, 84)));
        //Heading to the east
        assertEquals(new Point(-22, 85), drone.heading(Directions.EAST, Directions.NORTH, new Point(-23, 84)));
        assertEquals(new Point(-22, 83), drone.heading(Directions.EAST, Directions.SOUTH, new Point(-23, 84)));
        //Heading to the south
        assertEquals(new Point(24, -85), drone.heading(Directions.SOUTH, Directions.EAST, new Point(23, -84)));
        assertEquals(new Point(22, -85), drone.heading(Directions.SOUTH, Directions.WEST, new Point(23, -84)));
        //Heading to the west
        assertEquals(new Point(22, -83), drone.heading(Directions.WEST, Directions.NORTH, new Point(23, -84)));
        assertEquals(new Point(22, -85), drone.heading(Directions.WEST, Directions.SOUTH, new Point(23, -84)));
    }

}
