package fr.unice.polytech.si3.qgl.iabc.actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.2
 *          This class implements the scout action behavior.
 */
public class Scout extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;
    //the direction of the action.
    private Directions direction;

    /**
     * {"action":"scout","parameters":{"direction":"N"}}
     * Constructor of the class Scout.
     *
     * @param direction the direction to scout.
     */
    public Scout(Directions direction) {
        name = "scout";
        action = new JSONObject();
        action.put("action", name);
        action.put("parameters", new JSONObject().put("direction", direction));
        this.direction = direction;
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param men     the previous men to update.
     */
    @Override
    public void aftermath(String result, Context context, Men men) {
        super.aftermath(result, context, men);
        JSONObject stringToJSON = new JSONObject(result);
        JSONObject extrasObjects = stringToJSON.getJSONObject("extras");
        int altitude = extrasObjects.getInt("altitude");
        boolean unreachable = false;
        if (extrasObjects.has("unreachable"))
            unreachable = true;
        JSONArray jsonResources = extrasObjects.getJSONArray("resources");
        List<Resources> resources = new ArrayList<>();
        for (int i = 0; i < jsonResources.length(); i++) {
            resources.add(Resources.getResourceName(jsonResources.getString(i)));
        }
        men.scoutUpdate(direction, altitude, resources, unreachable);
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Getter of the direction.
     *
     * @return the direction used to do this action.
     */
    public Directions getDirection() {
        return direction;
    }

}
