import eu.ace_design.island.bot.IExplorerRaid;
import fr.unice.polytech.si3.qgl.iabc.Explorer;
import fr.unice.polytech.si3.qgl.iabc.actions.Land;
import fr.unice.polytech.si3.qgl.iabc.actions.Stop;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExplorerTest {

    private Explorer explorer;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        explorer = new Explorer();
        explorer.initialize(stringContext);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void implementsIExplorer() {
        assertTrue(explorer instanceof IExplorerRaid);
    }

    @Test
    public void initialize_drone() {
        assertEquals(explorer.getDrone().getHeading(), Directions.WEST);
    }

    @Test
    public void takeDecision_lowBudget() {
        explorer.getContext().spend(19990);
        assertEquals(explorer.takeDecision(), new Stop().toString());
    }

    @Test
    public void takeDecision_landingCondition() {
        assertNotEquals(new Land("RandomId", 66).toString(), explorer.getDrone().getIsBack());
    }

    @Test
    public void deliverFinalReport() {
        String res = explorer.deliverFinalReport();
        assertEquals("Creeks found:\n", res);
    }

    @Test
    public void finalReport() {
        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.ALPINE);
        biomesList.add(Biomes.TAIGA);
        explorer.getContext().getMap().getCreeksList().add(
                new Tile(new Point(15, -5), true, false, "858a79b7-54ab-4874-a8af-6f80679a8406", null, biomesList));
        explorer.getContext().getMap().getCreeksList().add(
                new Tile(new Point(-17, -9), true, false, "3882f0ad-d389-4546-a75a-69e7795b47a7", null, biomesList));
        explorer.getContext().getMap().getCreeksList().add(
                new Tile(new Point(0, -34), true, false, "9a9c08c8-bf2b-4b60-bc08-fbaa5b40acce", null, biomesList));
        explorer.takeDecision();
        explorer.acknowledgeResults("{\"cost\":6,\"extras\":{\"found\":\"GROUND\",\"range\":3}}");
        explorer.takeDecision();
        assertEquals("CREEK: 858a79b7-54ab-4874-a8af-6f80679a8406", explorer.deliverFinalReport());
    }

    @Test
    public void lastActionModifiedWhenOutOfBudget() {
        explorer.getContext().spend(explorer.getContext().getBudget().getBudgetLeft() - 10);
        explorer.takeDecision();
        assertTrue(explorer.getLastAction() instanceof Stop);
    }

}
