package fr.unice.polytech.si3.qgl.iabc.actions;

import org.json.JSONObject;

/**
 * @author Maven
 * @version 1.2
 *          This class implements the land action behavior.
 */
public class Land extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;
    //the creek to land to.
    private String creek;
    //the men.
    private int people;

    /**
     * {"action":"land","parameters":{"creek":"id","people":42}}
     * Constructor of the class Land.
     *
     * @param creek  the chosen creek.
     * @param people the men who are going to explore the island.
     */
    public Land(String creek, int people) {
        name = "land";
        JSONObject parameters = new JSONObject();
        parameters.put("creek", creek);
        parameters.put("people", people);
        action = new JSONObject();
        action.put("action", name);
        action.put("parameters", parameters);
        this.creek = creek;
        this.people = people;
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }


    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Getter of the creek which to land.
     *
     * @return the creek's id.
     */
    public String getCreek() {
        return creek;
    }

    /**
     * Getter or the number of people.
     *
     * @return the number of people.
     */
    public int getPeople() {
        return people;
    }

}
