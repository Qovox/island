package fr.unice.polytech.si3.qgl.iabc.strategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;

import java.util.List;

/**
 * @author Maven
 * @version 1.1
 *          Makes sure a strategy returns a decision
 */
public interface Strategy {

    Action takeDecision();

    List<Action> takeDecisionList();

}
