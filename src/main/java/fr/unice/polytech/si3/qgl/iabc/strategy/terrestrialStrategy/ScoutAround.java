package fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Scout;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap;
import fr.unice.polytech.si3.qgl.iabc.strategy.Strategy;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.0
 *          This part of the strategy scouts in different direction around
 *          the group of men to find ground to explore.
 */
public class ScoutAround implements Strategy {

    private Point menPosition;
    private TerrestrialMap map;

    /**
     * Constructor of ScoutAround.
     */
    public ScoutAround(TerrestrialMap map, Point menPosition) {
        this.map = map;
        this.menPosition = menPosition;
    }

    @Override
    public Action takeDecision() {
        return null;
    }

    @Override
    public List<Action> takeDecisionList() {
        List<Action> actions = new ArrayList<>();
        for (Directions dir : Directions.values()) {
            if (map.getTileByPositionWithDirection(menPosition, dir, 1).isPresent() &&
                    map.getTileByPositionWithDirection(menPosition, dir, 1).get().isExplored())
                continue;
            actions.add(new Scout(dir));
        }
        return actions;
    }

}
