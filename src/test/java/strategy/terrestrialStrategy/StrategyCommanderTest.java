package strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.map.*;
import fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy.StrategyCommander;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class StrategyCommanderTest {

    private StrategyCommander strategyCommanderEmpty;
    private StrategyCommander strategyCommander;

    @Before
    public void setUp() {
        strategyCommanderEmpty = new StrategyCommander(new Point(), new Inventory(new HashMap<>()), new TerrestrialMap(new ArrayList<>()));
        Map<Resources, Integer> resourcesToCollect = new HashMap<>();
        resourcesToCollect.put(Resources.WOOD, 8000);
        resourcesToCollect.put(Resources.FUR, 400);
        resourcesToCollect.put(Resources.QUARTZ, 100);
        resourcesToCollect.put(Resources.RUM, 5);
        List<Tile> tileList = new ArrayList<>();
        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.MANGROVE);
        tileList.add(new Tile(false, false, new Point(6, 0), biomesList));
        tileList.add(new Tile(false, false, new Point(1, 0), biomesList));
        tileList.add(new Tile(false, false, new Point(-1, 0), biomesList));
        tileList.add(new Tile(false, false, new Point(0, 1), biomesList));
        tileList.add(new Tile(false, false, new Point(0, -1), biomesList));
        strategyCommander = new StrategyCommander(new Point(), new Inventory(resourcesToCollect), new TerrestrialMap(tileList));
    }

    @After
    public void tearDown() {

    }

    @Test
    public void listSizeVerification() {
        assertEquals(4, strategyCommanderEmpty.takeDecision().size());
        assertEquals(4, strategyCommander.takeDecision().size());
        /*assertEquals(2, strategyCommander.takeDecision().size());
        assertEquals(3, strategyCommander.takeDecision().size());
        strategyCommander.exploreResults(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        assertEquals(2, strategyCommander.takeDecision().size());
        List<Resources> resourcesToCollect = new ArrayList<>();
        resourcesToCollect.add(Resources.WOOD);
        strategyCommander.scoutResults(Directions.EAST, resourcesToCollect);
        assertEquals(2, strategyCommander.takeDecision().size());*/
    }

    @Test
    public void glimpseResultTest() {
        List<List<Biomes>> biomesGlimpsed = new ArrayList<>();

        biomesGlimpsed.add(new ArrayList<>());
        biomesGlimpsed.add(new ArrayList<>());
        biomesGlimpsed.add(new ArrayList<>());
        biomesGlimpsed.add(new ArrayList<>());

        biomesGlimpsed.get(3).add(Biomes.OCEAN);
        assertFalse(strategyCommander.glimpseResults(Directions.NORTH, 4, biomesGlimpsed));

        biomesGlimpsed.get(3).clear();

        biomesGlimpsed.get(3).add(Biomes.SNOW);
        assertTrue(strategyCommander.glimpseResults(Directions.NORTH, 4, biomesGlimpsed));

        biomesGlimpsed.get(3).clear();

        biomesGlimpsed.get(3).add(Biomes.SNOW);
        biomesGlimpsed.get(3).add(Biomes.OCEAN);
        assertFalse(strategyCommander.glimpseResults(Directions.NORTH, 4, biomesGlimpsed));

        biomesGlimpsed.get(3).clear();

        biomesGlimpsed.get(3).add(Biomes.OCEAN);
        biomesGlimpsed.get(3).add(Biomes.SNOW);
        assertFalse(strategyCommander.glimpseResults(Directions.NORTH, 4, biomesGlimpsed));
    }

}
