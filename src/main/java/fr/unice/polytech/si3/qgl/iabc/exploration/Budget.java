package fr.unice.polytech.si3.qgl.iabc.exploration;

/**
 * @author Maven
 * @version 1.0
 *          This class provides budgetLeft management.
 */

public class Budget {

    //the left budget.
    private int budgetLeft;

    /**
     * Constructor of the class Budget.
     *
     * @param budgetLeft the budgetLeft.
     */
    public Budget(int budgetLeft) {
        this.budgetLeft = budgetLeft;
    }

    /**
     * This method allows to spend the budgetLeft after the action has been taken.
     *
     * @param cost the cost of the previous action.
     */
    public void spend(int cost) {
        budgetLeft -= cost;
    }

    /**
     * This method calculates whether the budgetLeft is high enough to continue the game or not.
     * For instance, if the next move of the men's group won't allow them to get back
     * to the ship, this method will stop the game before it happens.
     *
     * @return true if the game can continue, false otherwise.
     */
    public boolean check() {
        return budgetLeft >= 200;
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the budgetLeft.
     *
     * @return int representing the budgetLeft.
     */
    public int getBudgetLeft() {
        return budgetLeft;
    }
}
