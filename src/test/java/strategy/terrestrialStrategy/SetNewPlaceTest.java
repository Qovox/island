package strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.MoveTo;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap;
import fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy.SetNewPlace;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Maven
 *         Created by Thoma on 2/24/2017.
 */
public class SetNewPlaceTest {

    private SetNewPlace setNewPlace;

    @Before
    public void setUp() {
        setNewPlace = new SetNewPlace(new TerrestrialMap(), new Point(), new Radar(), Directions.WEST, false);
    }

    @Test
    public void simpleNewPlace() {
        List<Action> actionsExpected = setNewPlace.takeDecisionList();
        assertEquals(1, actionsExpected.size());
        assertEquals(new MoveTo(Directions.WEST).toString(), actionsExpected.get(0).toString());
    }

    @Test
    public void inDanger() {
        SetNewPlace setNewPlace = new SetNewPlace(new TerrestrialMap(), new Point(), new Radar(), Directions.EAST, true);

        List<Action> actionsExpected = setNewPlace.takeDecisionList();
        assertEquals(2, actionsExpected.size());
        assertEquals(new MoveTo(Directions.EAST).toString(), actionsExpected.get(0).toString());
        assertEquals(new MoveTo(Directions.SOUTH).toString(), actionsExpected.get(1).toString());
    }

}
