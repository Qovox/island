package fr.unice.polytech.si3.qgl.iabc.exploration;

import fr.unice.polytech.si3.qgl.iabc.map.Resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Maven
 * @version 1.0
 *          This class represents the inventory of the gathered resources
 *          while the exploration. It determines the priority order of resources
 *          to gather, and also indicates the amount of each resources gathered.
 */
public class Inventory {

    //the contract.
    private Map<Resources, Integer> contract;
    //the inventory.
    private Map<Resources, Integer> inventory;

    /**
     * Constructor of the class Inventory.
     *
     * @param contract the resources to collect. The contract never changes.
     */
    public Inventory(Map<Resources, Integer> contract) {
        this.contract = contract;
        inventory = new HashMap<>(contract);
    }

    /**
     * This method allows to know which resources are our priorities.
     *
     * @return the resources we need.
     */
    public Resources priorityResource() {
        int amount = -1;
        Resources priorityResource = null;
        for (Resources resource : contract.keySet()) {
            if (!resource.isManufactured() && (amount == -1 || contract.get(resource) < amount)) {
                amount = contract.get(resource);
                priorityResource = resource;
            }
        }
        return priorityResource;
    }

    /**
     * This method allows to update the inventory.
     *
     * @param resources the different resources.
     * @param amount    the amount of resources.
     * @return the new resources.
     */
    public Resources update(Resources resources, int amount) {
        inventory.put(resources, inventory.get(resources) - amount);
        if (inventory.get(resources) <= 0) {
            contract.remove(resources);
            inventory.remove(resources);
            return priorityResource();
        } else return resources;
    }

    public Map<Resources, Integer> checkTransform() {
        Map<Resources, Integer> craft = new HashMap<>();
        for (Resources res : getResourcesToCollect()) {
            if (res.isManufactured()) {
                for (Resources resToCraft : res.getRecipe().keySet()) {
                    if (!inventory.containsKey(resToCraft) ||
                            ((contract.get(resToCraft) - inventory.get(resToCraft)) >= res.getRecipe().get(resToCraft) * contract.get(res))) {
                        craft.put(resToCraft, res.getRecipe().get(resToCraft));
                    } else return new HashMap<>();
                }
                return craft;
            }
        }
        return new HashMap<>();
    }

    public boolean noRessourcesCollected() {
        return contract.equals(inventory);
    }

    /**
     * Getter of the needed resources.
     *
     * @return the list of the needed resources.
     */
    public List<Resources> getResourcesToCollect() {
        List<Resources> resourcesList = new ArrayList<>();
        resourcesList.addAll(inventory.keySet());
        return resourcesList;
    }

    /**
     * Getter of the inventory of the men.
     *
     * @return the amount of resources associated with the resources.
     */
    public Map<Resources, Integer> getInventory() {
        return inventory;
    }

}
