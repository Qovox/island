package fr.unice.polytech.si3.qgl.iabc.exploration;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.Stop;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Transform;
import fr.unice.polytech.si3.qgl.iabc.map.*;
import fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy.StrategyCommander;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Men {

    private Point position;
    private Radar radar;
    private Inventory inventory;
    private TerrestrialMap terrestrialMap;
    private List<Action> actions;
    private StrategyCommander interpreter;

    /**
     * Constructor of the class Men.
     */
    public Men(Point position, Inventory inventory, Radar radar) {
        this.position = position;
        this.radar = radar;
        this.inventory = inventory;
        terrestrialMap = new TerrestrialMap();
        actions = new ArrayList<>();
        interpreter = new StrategyCommander(position, inventory, terrestrialMap);
    }

    /**
     * This method should determine from which part of the strategy the men
     * need help given the current progress of the exploration.
     *
     * @return the action the strategy wants the men to do.
     */
    public Action takeDecision() {
        Map<Resources, Integer> resToTransform = inventory.checkTransform();
        if (!resToTransform.isEmpty()) {
            actions.add(new Transform(resToTransform));
        }
        if (inventory.getResourcesToCollect().isEmpty()) {
            return new Stop();
        }
        if (!actions.isEmpty()) {
            return actions.remove(actions.size() - 1);
        }
        interpreter.updateAfterAction(position, terrestrialMap, radar, inventory);
        actions = interpreter.takeDecision();
        return actions.remove(actions.size() - 1);
    }

    /**
     * This method allows to update the move to action.
     *
     * @param direction the direction of the move to action.
     */
    public void moveUpdate(Directions direction) {
        Point newPosition = new Point(position);
        switch (direction) {
            case NORTH:
                newPosition.translate(0, 1);
                position = newPosition;
                break;
            case EAST:
                newPosition.translate(1, 0);
                position = newPosition;
                break;
            case SOUTH:
                newPosition.translate(0, -1);
                position = newPosition;
                break;
            case WEST:
                newPosition.translate(-1, 0);
                position = newPosition;
                break;
        }
        if (!terrestrialMap.getTileByPosition(position).isPresent()) {
            terrestrialMap.addTile(new Tile(false, false, position, new ArrayList<>()));
        }
    }

    /**
     * This method allows to update the exploit action.
     *
     * @param resource the exploited resource.
     * @param amount   the amount of resources.
     */
    public void exploitUpdate(Resources resource, int amount) {
        inventory.update(resource, amount);
    }

    /**
     * This method allows to update the explore action of the men.
     *
     * @param amount    the amount of resources.
     * @param resources the resources.
     * @param cond      the condition of the exploration.
     */
    public void exploreUpdate(List<Amount> amount, List<Resources> resources, List<Conditions> cond) {
        if (terrestrialMap.getTileByPosition(position).isPresent())
            terrestrialMap.getTileByPosition(position).get().setIsExplored(true);
        else terrestrialMap.addTile(new Tile(true, false, position, new ArrayList<>()));
        interpreter.exploreResults(amount, resources, cond);
    }

    /**
     * This method allows to update the glimpse action.
     *
     * @param direction      the direction of the glimpse action.
     * @param range          the range of it.
     * @param biomesGlimpsed the list of the glimpsed biomes.
     */
    public void glimpseUpdate(Directions direction, int range, List<List<Biomes>> biomesGlimpsed) {
        terrestrialMap.glimpseUpdate(position, direction, biomesGlimpsed.size(), biomesGlimpsed);
        if (interpreter.glimpseResults(direction, biomesGlimpsed.size(), biomesGlimpsed))
            actions.clear();
    }

    /**
     * This method allows to update the scout action.
     *
     * @param direction the direction of the scout action.
     * @param altitude  the altitude of the action.
     * @param resources the resources.
     */
    public void scoutUpdate(Directions direction, int altitude, List<Resources> resources, boolean unreachable) {
        if (terrestrialMap.getTileByPosition(position).isPresent())
            terrestrialMap.getTileByPosition(position).get().setIsScouted(true);
        else terrestrialMap.addTile(new Tile(false, true, position, new ArrayList<>()));
        if (interpreter.scoutResults(direction, resources))
            actions.clear();
        if (unreachable) {
            actions.clear();
            actions.add(new Stop());
        }
    }

    /**
     * This method allows to update the transform action.
     *
     * @param resource     the resources.
     * @param production   the production of
     * @param manufactured the manufactured resources.
     */
    public void transformUpdate(Map<Resources, Integer> resource, int production, Resources manufactured) {
        inventory.update(manufactured, production);
    }

    /**
     * Getter of the position of the men.
     *
     * @return their coordinates (x,y).
     */
    public Point getPosition() {
        return position;
    }

    public Inventory getInventory() {
        return inventory;
    }

}
