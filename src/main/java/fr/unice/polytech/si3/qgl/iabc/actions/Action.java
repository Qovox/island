package fr.unice.polytech.si3.qgl.iabc.actions;

import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import org.json.JSONObject;

/**
 * @author Maven
 * @version 1.2
 *          This class contains the main methods each action should implement.
 */
public class Action {

    //the name of the actions.
    private static final String name = "";

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param drone   the previous drone to update.
     */
    public void aftermath(String result, Context context, Drone drone) {
        JSONObject stringToJSON = new JSONObject(result);
        int cost = stringToJSON.getInt("cost");

        context.spend(cost);
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param men     the previous group of men to update.
     */
    public void aftermath(String result, Context context, Men men) {
        JSONObject stringToJSON = new JSONObject(result);
        int cost = stringToJSON.getInt("cost");

        context.spend(cost);
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    public String getName() {
        return name;
    }

}
