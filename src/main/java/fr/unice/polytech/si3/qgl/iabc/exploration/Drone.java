package fr.unice.polytech.si3.qgl.iabc.exploration;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.map.Compass;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import fr.unice.polytech.si3.qgl.iabc.strategy.aerialStrategy.AerialStrategy;

import java.awt.*;

/**
 * @author Maven
 * @version 2.0
 *          This class manages the drone. It allows the drone to explore and
 *          to take decisions.
 */
public class Drone {

    //the radar of the drone.
    private Radar radar;
    //the position of the drone on the map (x,y).
    private Point position;
    //the used compass to handle the drone's direction.
    private Compass compass;
    //the heading of the drone.
    private Directions heading;
    //the boolean to know whether the drone is back or not.
    private boolean isBack = false;
    //the aerial strategy.
    private AerialStrategy aerialStrategy;

    /**
     * Constructor of the class Drone.
     */
    public Drone() {
        position = new Point(0, 0);
        radar = new Radar();
        aerialStrategy = new AerialStrategy(this);
        compass = new Compass();
    }

    /**
     * This method allows the drone to take a decision.
     *
     * @return the taken decision(action).
     */
    public Action takeDecision() {
        return aerialStrategy.takeDecision();
    }

    /**
     * This method allows to turn the compass left.
     *
     * @param heading the actual heading.
     * @return the pointed direction.
     */
    public Directions left(Directions heading) {
        return compass.left(heading);
    }

    /**
     * This method allows to turn the compass right.
     *
     * @param heading the actual heading.
     * @return the pointed direction.
     */
    public Directions right(Directions heading) {
        return compass.right(heading);
    }

    /**
     * This method allows the drone to fly.
     *
     * @param direction the direction to fly.
     * @param position  the (x,y) coordinates.
     * @return the new position of the drone after the fly.
     */
    public Point fly(Directions direction, Point position) {
        Point newPosition = new Point(position);

        switch (direction) {
            case NORTH:
                newPosition.translate(0, 1);
                return newPosition;
            case EAST:
                newPosition.translate(1, 0);
                return newPosition;
            case SOUTH:
                newPosition.translate(0, -1);
                return newPosition;
            case WEST:
                newPosition.translate(-1, 0);
                return newPosition;
            default:
                return newPosition;
        }
    }

    /**
     * This method allows the drone to head to the wanted direction.
     *
     * @param headingDirection the direction of the heading.
     * @param heading          current drone's heading
     * @param position         the (x,y) coordinates.
     * @return the new position of the drone after the heading.
     */
    public Point heading(Directions headingDirection, Directions heading, Point position) {
        switch (heading) {
            case NORTH:
                return headingFromNorth(headingDirection, position);
            case EAST:
                return headingFromEast(headingDirection, position);
            case SOUTH:
                return headingFromSouth(headingDirection, position);
            case WEST:
                return headingFromWest(headingDirection, position);
            default:
                return new Point(position);
        }
    }

    /**
     * Modifies the direction of the drone from an initial North heading.
     *
     * @param headingDirection the direction of the heading.
     * @param position         the (x,y) coordinates.
     * @return the new position of the drone after the heading.
     */
    private Point headingFromNorth(Directions headingDirection, Point position) {
        Point newPosition = new Point(position);

        switch (headingDirection) {
            case EAST:
                newPosition.translate(1, 1);
                return newPosition;
            case WEST:
                newPosition.translate(-1, 1);
                return newPosition;
            default:
                return newPosition;
        }
    }

    /**
     * Modifies the direction of the drone from an initial East heading.
     *
     * @param headingDirection the direction of the heading.
     * @param position         the (x,y) coordinates.
     * @return the new position of the drone after the heading.
     */
    private Point headingFromEast(Directions headingDirection, Point position) {
        Point newPosition = new Point(position);

        switch (headingDirection) {
            case NORTH:
                newPosition.translate(1, 1);
                return newPosition;
            case SOUTH:
                newPosition.translate(1, -1);
                return newPosition;
            default:
                return newPosition;
        }
    }

    /**
     * Modifies the direction of the drone from an initial South heading.
     *
     * @param headingDirection the direction of the heading.
     * @param position         the (x,y) coordinates.
     * @return the new position of the drone after the heading.
     */
    private Point headingFromSouth(Directions headingDirection, Point position) {
        Point newPosition = new Point(position);

        switch (headingDirection) {
            case EAST:
                newPosition.translate(1, -1);
                return newPosition;
            case WEST:
                newPosition.translate(-1, -1);
                return newPosition;
            default:
                return newPosition;
        }
    }

    /**
     * Modifies the direction of the drone from an initial West heading.
     *
     * @param headingDirection the direction of the heading.
     * @param position         the (x,y) coordinates.
     * @return the new position of the drone after the heading.
     */
    private Point headingFromWest(Directions headingDirection, Point position) {
        Point newPosition = new Point(position);

        switch (headingDirection) {
            case NORTH:
                newPosition.translate(-1, 1);
                return newPosition;
            case SOUTH:
                newPosition.translate(-1, -1);
                return newPosition;
            default:
                return newPosition;
        }
    }

    //========================
    //   Getters and setters
    //========================

    public void setPosition(Point position) {
        this.position = position;
    }

    /**
     * Getter of the drone's current position.
     *
     * @return the current position of the drone.
     */
    public Point getPosition() {
        return position;
    }

    /**
     * Setter of the heading.
     */
    public void setHeading(Directions heading) {
        this.heading = heading;
    }

    /**
     * Getter of drone's current heading.
     *
     * @return String the current heading of the drone.
     */
    public Directions getHeading() {
        return heading;
    }

    /**
     * Getter of the radar.
     *
     * @return the radar of the drone.
     */
    public Radar getRadar() {
        return radar;
    }

    /**
     * Setter of isBack.
     *
     * @param isBack
     */
    public void setIsBack(boolean isBack) {
        this.isBack = isBack;
    }

    /**
     * Boolean checking whether it's back or not.
     *
     * @return isBack.
     */
    public boolean getIsBack() {
        return isBack;
    }

    /**
     * Getter of the aerial Strategy.
     *
     * @return
     */
    public AerialStrategy getAerialStrategy() {
        return aerialStrategy;
    }

}