# ISLAND PROJECT


## Team Information

  * Identifier: `BC`
  * Team name: Maven
  * Members:
    *  [GREAUX Thomas](thomas.greaux@etu.unice.fr)
    *  [REDON Alexandre](alexandre.redon@etu.unice.fr)
    *  [LARA Jeremy](jeremy.lara@etu.unice.fr)
    *  [UNG Eric](eric.ung@etu.unice.fr)

## Features Change log


### Week 45

Installation of Maven and setup of Git.

Objective: The bot stop the game as soon as it begins
    Seconday objective: find a creek

Result: Could not enter the game, error in the tag.


### Week 46

Objective: Enter the game, the bot find a creek and then stop

Result: Could not enter the game, build failed, some tests did pass.


### Week 47

Objective : Enter the game, find a creek.
	Secondary objectif: Find the distress site.
	Tertiary objectif: Find the closest creek to distress site.
    
Result: Entered the game, incomplete enum, the drone crashed.


### Week 48

Objective: Find a creek and the distress site.

Result: Could not enter the game, didn't implement ExplorerRaid.


### Week 49

Objective: Find a creek and the distress site.

Result: Could not enter the game, build failed.


### Week 50

Objective: Find a creek and the distress site.

Result: Didn't find a creek. Stopped immediately the drone.


### Week 51 No mission.




### Week 52

Objective: Find a creek and the distress site.

Result: Found the closest creek and the distress site.



### Week 01

Objective: Find a creek and the distress site.

Result: Found a creek. Didn't find the distress site.


### Week 02

Objective: Find the closest creek and the distress site.

Result: Distress site and closest creek found.


### Week 05

Objective: Collect one type of primary resources.

Result: Distress site and closest creek found.


### Week 06

Objective: Collect one type of primary resources.

Result: Distress site and closest creek found.


### Week 07

Objective: Collect one type of primary resources.

Result: failed. Only creek found.


### Week 08

Objective: Collect one type of primary resources.

Result: failed. Only creek found.
 
 
### Week 09
 
Objective: Complete a contract.
 
Result: Failed. Crashing. Collected resources though. (stop cost 100+ and we had less)
 
 
### Week 10
 
Objective: Complete a line of the contract.
 
Result: Completed a line of the contract, crashed though.
The crash is due to the command: glimpse.

### Week 11

Objective: Complete several lines of the contract.

Result: Completed only 30% of a line of the contract, drowned though.



### Week12

Objective: Complete the contract.

Result: 


