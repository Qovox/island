package actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Scout;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import fr.unice.polytech.si3.qgl.iabc.map.Resources;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ScoutTest {

    private Men men;
    private Context context;
    private Drone drone;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 10,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":6000,\"resource\":\"WOOD\"}," +
                "{ \"amount\":2000,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        men = new Men(new Point(), new Inventory(new HashMap<>()), new Radar());
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void scoutToString() {
        Scout scout = new Scout(Directions.EAST);
        String scoutToString = "{\"action\":\"scout\",\"parameters\":{\"direction\":\"E\"}}";
        String BadScoutToString = "{\"action\":\"scout\",\"parameters\":{\"direction\":\"S\"}}";
        assertEquals(scoutToString, scout.toString());
        assertNotEquals(BadScoutToString, scout.toString());
        assertEquals("scout", scout.getName());
        assertEquals(Directions.EAST, scout.getDirection());
    }


    @Test
    public void scoutAftermath() {
        String result = "{ \"cost\": 5, \"extras\": { \"altitude\": 1, \"resources\": [\"FUR\", \"WOOD\"] }, \"status\": \"OK\" }";
        new Scout(Directions.NORTH).aftermath(result, context, men);
        assertEquals(19995, context.getBudget().getBudgetLeft());
        List<Resources> resourcesList = new ArrayList<>();
        resourcesList.add(Resources.FUR);
        resourcesList.add(Resources.WOOD);
        int lastIndex = context.getMap().getMap().size() - 1;

    }

}
