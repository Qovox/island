package fr.unice.polytech.si3.qgl.iabc.map;

/**
 * Enumeration of the different available conditions in the Island game.
 * We can have easy, fair or harsh conditions to exploit a resource.
 */
public enum Conditions {

    HARSH, FAIR, EASY;

    /**
     * @return a string representation of the object name.
     */
    @Override
    public String toString() {
        return this.name();
    }

    /**
     * This method allows an Enum to get its real name.
     *
     * @return the real name of the Enum.
     */
    public static Conditions getConditionName(String condition) {
        for (Conditions conditions : Conditions.values()) {
            if (conditions.toString().equals(condition)) {
                return conditions;
            }
        }
        return null;
    }
}
