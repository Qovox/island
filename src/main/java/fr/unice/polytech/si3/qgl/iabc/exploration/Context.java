package fr.unice.polytech.si3.qgl.iabc.exploration;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.Land;
import fr.unice.polytech.si3.qgl.iabc.map.AerialMap;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;
import fr.unice.polytech.si3.qgl.iabc.strategy.PhaseTransition;
import fr.unice.polytech.si3.qgl.iabc.strategy.aerialStrategy.AerialStrategy;

import java.util.List;

/**
 * This class represents the context which the
 * explorer has to use to explore. This context includes a
 * budget, a map, and the group of men.
 */
public class Context {

    //the initialization.
    private Initialize initialize;
    //the budget of the game.
    private Budget budget;
    //the inventory of the men.
    private Inventory inventory;
    //the aerial map.
    private AerialMap aerialMap;
    //the aerial strategy.
    private AerialStrategy aerialStrategy;

    /**
     * Constructor of the class exploration.
     *
     * @param context the context.
     * @param drone   the drone.
     */
    public Context(String context, Drone drone) {
        initialize = new Initialize(context);
        inventory = initialize.initialize();
        budget = new Budget(initialize.getBudget());
        aerialMap = new AerialMap();
        drone.setHeading(initialize.getHeading());
        aerialStrategy = drone.getAerialStrategy();
    }

    /**
     * This method allows to update the drone (fly information).
     *
     * @param drone the flying drone.
     */
    public void flyUpdate(Drone drone) {
        aerialStrategy.flyUpdate(drone);
        aerialStrategy.updateOnMap(aerialMap, null, "fly", "", -1);
    }

    /**
     * This method allows to update the drone (heading information).
     *
     * @param drone            the flying drone.
     * @param headingDirection the direction of the heading.
     */
    public void headingUpdate(Drone drone, Directions headingDirection) {
        aerialStrategy.headingUpdate(drone, headingDirection);
        aerialStrategy.updateOnMap(aerialMap, null, "heading", "", -1);
    }

    /**
     * This method allows to know whether the budget is >= 100 or not.
     *
     * @return true if budget >=100, false otherwise.
     */
    public boolean check() {
        return budget.check();
    }

    /**
     * This method allows to update the budget due to the differents actions.
     *
     * @param cost of each action.
     */
    public void spend(int cost) {
        budget.spend(cost);
    }


    /**
     * This method allows the drone to do an echo.
     *
     * @param drone            the current drone.
     * @param commandDirection the direction used to perform the echo.
     * @param range            an integer which specifies the range of the previous echo.
     * @param found            a string which specifies if ground has been found or not.
     */
    public void mapEcho(Drone drone, Directions commandDirection, int range, String found) {
        aerialMap.mapEcho(drone, commandDirection, range, found);
        aerialStrategy.updateOnMap(aerialMap, commandDirection, "echo", found, range);
    }

    /**
     * This method allows the drone to do a scan.
     *
     * @param drone      the current drone.
     * @param biomesList the biomes' list of the scanned tile.
     * @param creeksList the creeks' list of the scanned tile.
     * @param sites      the scanned tile's site if it exists.
     */
    public void mapScan(Drone drone, List<Biomes> biomesList, List<String> creeksList, String sites) {
        aerialMap.mapScan(drone.getPosition(), biomesList, creeksList, sites);
        aerialStrategy.updateOnMap(aerialMap, null, "scan", "", -1);
    }

    /**
     * This method allows to correctly go from the aerial phase to the ground one.
     *
     * @return the action of landing to start the ground exploration.
     */
    public Action transition() {
        PhaseTransition phaseTransition = new PhaseTransition();
        return new Land(phaseTransition.landingTile(aerialMap), nbrOfMenToUse());
    }

    /**
     * This method allows to calculate the right number of men to use on the island.
     * (at least, 1 man will always stay on the boat)
     *
     * @return the number of men.
     */
    public int nbrOfMenToUse() {
        int nbrMen = initialize.getMen() - 1;
        if (nbrMen > 7)
            nbrMen = 7;
        return nbrMen;
    }

    /**
     * This method allows to get the creeks.
     *
     * @return the creeks.
     */
    public String getCreeks() {
        StringBuilder creeks = new StringBuilder();
        if (!aerialMap.getCreeksList().isEmpty()) {
            for (Tile creek : aerialMap.getCreeksList()) {
                creeks.append("CREEK:");
                creeks.append(creek.idCreek());
                creeks.append("\n");
            }
            return creeks.toString();
        }
        return "";
    }

    /**
     * This method allows to get the emergency site.
     *
     * @return the site id.
     */
    public String getEmergency() {
        return aerialMap.getEmergencySite().isPresent() ?
                "EMERGENCY:" + aerialMap.getEmergencySite().get().idEmergencySite() : "";
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the budget.
     *
     * @return the budget.
     */
    public Budget getBudget() {
        return budget;
    }

    /**
     * Getter of the aerial map.
     *
     * @return the aerial map.
     */
    public AerialMap getMap() {
        return aerialMap;
    }

    /**
     * Getter of the inventory.
     *
     * @return the inventory.
     */
    public Inventory getInventory() {
        return inventory;
    }

}
