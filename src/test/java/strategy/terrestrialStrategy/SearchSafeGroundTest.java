package strategy.terrestrialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.menAction.Glimpse;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap;
import fr.unice.polytech.si3.qgl.iabc.strategy.terrestrialStrategy.SearchSafeGround;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SearchSafeGroundTest {

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void takeDecisionList() {
        SearchSafeGround searchSafeGround = new SearchSafeGround(new TerrestrialMap(), new Point(), 2);
        List<Action> actionList = searchSafeGround.takeDecisionList();

        assertEquals(4, actionList.size());
        assertEquals(new Glimpse(Directions.WEST, 2).toString(), actionList.get(3).toString());
        assertEquals(new Glimpse(Directions.EAST, 2).toString(), actionList.get(2).toString());
        assertEquals(new Glimpse(Directions.SOUTH, 2).toString(),actionList.get(1).toString());
        assertEquals(new Glimpse(Directions.NORTH, 2).toString(), actionList.get(0).toString());
    }

}
