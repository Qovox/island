package fr.unice.polytech.si3.qgl.iabc.strategy.aerialStrategy;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Echo;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Fly;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Heading;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Scan;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.AerialMap;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;
import fr.unice.polytech.si3.qgl.iabc.strategy.Strategy;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Maven
 * @version 1.1
 *          Handle the drone / flight phase
 */
public class AerialStrategy implements Strategy {

    private Drone drone;
    private AerialMap aerialMap;
    private List<Action> actions;
    private Directions explorationDirection;
    private Optional<Tile> currentTile = Optional.empty();
    private boolean canExplore = false;
    private boolean exploring = false;
    private boolean exploringMore = false;
    private boolean lineGround = false;
    private boolean interGround = false;
    private boolean positioning = false;
    private int range = 0;
    private String found = "";
    private static final String OOR = "OUT_OF_RANGE";
    private static final String GROUND = "GROUND";

    /**
     * Constructor of AerialStrategy.
     *
     * @param drone the used drone.
     */
    public AerialStrategy(Drone drone) {
        this.drone = drone;
        aerialMap = new AerialMap();
        actions = new ArrayList<>();
    }

    /**
     * This method allows the drone to take a decision given a situation.
     *
     * @return String the decision taken.
     */
    @Override
    public Action takeDecision() {
        if (!actions.isEmpty()) {
            return actions.remove(actions.size() - 1);
        }
        if (positioning) {
            return exploreMore();
        }
        if (canExplore) {
            return scanMap();
        } else if (unknown()) {
            return solveUnknown();
        } else {
            return searchGround();
        }
    }

    @Override
    public List<Action> takeDecisionList() {
        return null;
    }

    /**
     * This method allows the takeDecision method to take a decision when
     * drone's surrounding is unknown.
     *
     * @return a boolean which confirm if the drone is surrounded by unknown tiles.
     */
    public boolean unknown() {
        drone.getRadar().updateSurroundingTile(drone.getPosition(), drone.getHeading(), aerialMap);
        return !(drone.getRadar().getForward().isPresent() ||
                drone.getRadar().getLeft().isPresent() ||
                drone.getRadar().getRight().isPresent());
    }

    /**
     * This method solves the unknown state.
     *
     * @return an echo in the current heading.
     */
    public Action solveUnknown() {
        actions.add(new Echo(drone.left(drone.getHeading())));
        actions.add(new Echo(drone.right(drone.getHeading())));
        return new Echo(drone.getHeading());
    }

    /**
     * This method tries to find the ground.
     *
     * @return a fly or a heading.
     */
    public Action searchGround() {
        actions.add(new Echo(drone.getRadar().relativeMaxDirection(drone.getHeading(), drone.getPosition())));
        return new Fly();
    }

    /**
     * This method allows the drone to scan until there is no ground then he'll turn.
     *
     * @return the fly action if the current tile is already scanned,
     * the scan action if the drone is still on the ground and the current tile
     * has not been scanned.
     */
    public Action scanMap() {
        if (!exploring) {
            exploring = true;
            lineGround = true;
            explorationDirection = drone.getRadar().relativeMaxDirection(drone.getHeading(), drone.getPosition());
            return new Scan();
        }
        if ((currentTile.isPresent() && currentTile.get().isGround()) || (GROUND.equals(found) && interGround)) {
            lineGround = true;
            actions.add(new Scan());
            return new Fly();
        } else return particularScanMap();
    }

    /**
     * This method allows the drone to keep scanning the aerialMap when the drone is not on the ground anymore.
     *
     * @return the echo action, or a conditional heading.
     */
    public Action particularScanMap() {
        if (currentTile.isPresent() && !currentTile.get().isGround() && !OOR.equals(found)) {
            return new Echo(drone.getHeading());
        } else if (OOR.equals(found) && lineGround) {
            return turnAround();
        } else {
            exploring = false;
            exploringMore = true;
            return exploreMorePositioning();
        }
    }

    /**
     * This method allows the drone to turn around the wanted direction.
     *
     * @return the different actions wanted.
     * @see Radar
     */
    public Action turnAround() {
        if (range >= 4) {
            range = -1;
            actions.add(new Fly());
            actions.add(new Fly());
            return new Fly();
        } else {
            this.found = "";
            lineGround = false;
            switch (explorationDirection) {
                case NORTH:
                    return turnAroundNorth();
                case EAST:
                    return turnAroundEast();
                case SOUTH:
                    return turnAroundSouth();
                case WEST:
                    return turnAroundWest();
                default:
                    return new Scan();
            }
        }
    }

    /**
     * This method allows to head to North (from East or West) with scanning actions.
     *
     * @return the action of heading.
     */
    public Action turnAroundNorth() {
        switch (drone.getHeading()) {
            case EAST:
                actions.add(new Scan());
                actions.add(new Heading(drone.left(drone.left(drone.getHeading()))));
                actions.add(new Scan());
                return new Heading(drone.left(drone.getHeading()));
            case WEST:
                actions.add(new Scan());
                actions.add(new Heading(drone.right(drone.right(drone.getHeading()))));
                actions.add(new Scan());
                return new Heading(drone.right(drone.getHeading()));
            default:
                return new Scan();
        }
    }

    /**
     * This method allows to head East (from North or South) with scanning actions.
     *
     * @return the action of heading.
     */
    public Action turnAroundEast() {
        switch (drone.getHeading()) {
            case NORTH:
                actions.add(new Scan());
                actions.add(new Heading(drone.right(drone.right(drone.getHeading()))));
                actions.add(new Scan());
                return new Heading(drone.right(drone.getHeading()));
            case SOUTH:
                actions.add(new Scan());
                actions.add(new Heading(drone.left(drone.left(drone.getHeading()))));
                actions.add(new Scan());
                return new Heading(drone.left(drone.getHeading()));
            default:
                return new Scan();
        }
    }

    /**
     * This method allows to head South (from East or West) with scanning actions.
     *
     * @return the action of heading.
     */
    public Action turnAroundSouth() {
        switch (drone.getHeading()) {
            case EAST:
                actions.add(new Scan());
                actions.add(new Heading(drone.right(drone.right(drone.getHeading()))));
                actions.add(new Scan());
                return new Heading(drone.right(drone.getHeading()));
            case WEST:
                actions.add(new Scan());
                actions.add(new Heading(drone.left(drone.left(drone.getHeading()))));
                actions.add(new Scan());
                return new Heading(drone.left(drone.getHeading()));
            default:
                return new Scan();
        }
    }

    /**
     * This method allows to head West (from North or South) with scanning actions.
     *
     * @return the action of heading.
     */
    public Action turnAroundWest() {
        switch (drone.getHeading()) {
            case NORTH:
                actions.add(new Scan());
                actions.add(new Heading(drone.left(drone.left(drone.getHeading()))));
                actions.add(new Scan());
                return new Heading(drone.left(drone.getHeading()));
            case SOUTH:
                actions.add(new Scan());
                actions.add(new Heading(drone.right(drone.right(drone.getHeading()))));
                actions.add(new Scan());
                return new Heading(drone.right(drone.getHeading()));
            default:
                return new Scan();
        }
    }

    public Action exploreMorePositioning() {
        if (range >= 3 &&
                drone.getHeading() != drone.getRadar().relativeMaxDirection(
                        drone.getRadar().relativeMaxDirection(
                                drone.getHeading(), drone.getPosition()), drone.getPosition())) {
            for (int i = 0; i < range - 3; i++) {
                actions.add(new Fly());
            }
            positioning = true;
            return new Fly();
        } else {
            return exploreMore();
        }
    }

    /**
     * This method allows to explore more in the wanted direction
     * based on the relative max direction calculated.
     *
     * @return the action of heading in the wanted direction.
     * @see Radar
     */
    public Action exploreMore() {
        this.found = "";
        positioning = false;
        actions.add(new Heading(drone.getRadar().relativeMaxDirection(
                drone.getRadar().relativeMaxDirection(drone.getHeading(), drone.getPosition()), drone.getPosition())));
        actions.add(new Fly());
        return new Heading(drone.getRadar().relativeMaxDirection(drone.getHeading(), drone.getPosition()));
    }

    /**
     * This method allows the drone to know the aerialMap he's discovering and to stop exploring when
     * we have found a creek, the emergency site and we do not need to explore more.
     *
     * @param aerialMap the current aerialMap.
     */
    public void updateOnMap(AerialMap aerialMap, Directions echoDirection, String actionName, String found, int range) {
        this.aerialMap = aerialMap;
        this.range = range;
        currentTile = aerialMap.getTileByPosition(drone.getPosition());

        if (canExplore) {
            updateState(actionName, found);
        } else {
            goToTheGround(echoDirection, found);
        }
        if ((currentTile.isPresent() && currentTile.get().isGround()) ||
                (GROUND.equals(found) && drone.getHeading().equals(echoDirection))) {
            canExplore = true;
        }
        if (!aerialMap.getCreeksList().isEmpty()) {
            drone.setIsBack(true);
        }
    }

    /**
     * This method allows to update the state of the drone after scanning the aerialmap.
     *
     * @param actionName the previous action's name.
     * @param found      "OUT_OF_RANGE" if no ground has been found, "GROUND" if ground
     *                   has been found, "" otherwise.
     */
    public void updateState(String actionName, String found) {
        if ("scan".equals(actionName) && GROUND.equals(this.found) &&
                currentTile.isPresent() && currentTile.get().isGround()) {
            interGround = false;
        } else if ("echo".equals(actionName)) {
            if (!GROUND.equals(found) && currentTile.isPresent() && !currentTile.get().isGround()) {
                interGround = false;
                this.found = found;
            } else {
                interGround = true;
                this.found = found;
            }
        }
    }

    /**
     * This method allows to go to the ground found by the drone with echos.
     *
     * @param echoDirection the last echoDirection if it exists, null otherwise.
     * @param found         "OUT_OF_RANGE" if no ground has been found, "GROUND" if ground
     *                      has been found, "" otherwise.
     */
    public void goToTheGround(Directions echoDirection, String found) {
        drone.getRadar().updateSurroundingTile(drone.getPosition(), drone.getHeading(), aerialMap);

        if (GROUND.equals(found) && !drone.getHeading().equals(echoDirection)) {
            actions.add(new Echo(echoDirection));
            actions.add(new Heading(echoDirection));
        } else if (GROUND.equals(found) && drone.getHeading().equals(echoDirection)) {
            canExplore = true;
            this.found = found;
            interGround = true;
        }
    }

    /**
     * This method allows to manage the location of the drone.
     * doing a the action fly on a quadrilled aerialMap.
     */
    public void flyUpdate(Drone drone) {
        drone.setPosition(new Point(drone.fly(drone.getHeading(), drone.getPosition())));
    }

    /**
     * This method updates coordinates and direction after doing a heading.
     *
     * @param headingDirection the direction used to change the heading and the new heading.
     */
    public void headingUpdate(Drone drone, Directions headingDirection) {
        drone.setPosition(new Point(drone.heading(headingDirection, drone.getHeading(), drone.getPosition())));
        drone.setHeading(headingDirection);
    }

}
