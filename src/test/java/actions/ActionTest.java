package actions;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.actions.droneAction.Heading;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class ActionTest {

    private Men men;
    private Drone drone;
    private Context context;

    @Before
    public void setUp() {
        men = new Men(new Point(), new Inventory(new HashMap<>()), new Radar());
        String stringContext = "{\"men\": 38,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":600,\"resource\":\"WOOD\"}," +
                "{ \"amount\":200,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void getName() {
        Action action = new Action();
        assertEquals("", action.getName());
        Action heading = new Heading(Directions.NORTH);
        assertEquals("heading", heading.getName());
    }

    @Test
    public void droneAftermath() {
        String result = "{\"cost\":15}";
        Action action = new Action();
        action.aftermath(result, context, drone);
        assertEquals(19985, context.getBudget().getBudgetLeft());
    }

    @Test
    public void menAftermath() {
        String result = "{\"cost\":30}";
        Action action = new Action();
        action.aftermath(result, context, men);
        assertEquals(19970, context.getBudget().getBudgetLeft());
    }

}
