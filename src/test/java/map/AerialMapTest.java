package map;

import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Tile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AerialMapTest {

    private fr.unice.polytech.si3.qgl.iabc.map.AerialMap aerialMap;
    private Drone drone;

    @Before
    public void setUp() {
        aerialMap = new fr.unice.polytech.si3.qgl.iabc.map.AerialMap();
        drone = new Drone();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void echoMapGROUND() {
        aerialMap.mapEcho(drone, Directions.EAST, 2, "GROUND");
        assertEquals(2, aerialMap.getMap().size());
        assertTrue(aerialMap.getTileByPosition(new Point(1, 0)).isPresent());
        assertTrue(aerialMap.getTileByPosition(new Point(2, 0)).isPresent());
        assertTrue(aerialMap.getTileByPosition(new Point(2, 0)).get().isGround());
        assertEquals(Biomes.OCEAN, aerialMap.getMap().get(0).getBiomesList().get(0));
        assertTrue(aerialMap.getMap().get(1).getBiomesList().isEmpty());
    }

    @Test
    public void echoMapGROUNDAtRange0() {
        aerialMap.mapEcho(drone, Directions.EAST, 0, "GROUND");
        assertEquals(1, aerialMap.getMap().size());
        assertTrue(aerialMap.getTileByPosition(new Point(1, 0)).isPresent());
        assertTrue(aerialMap.getTileByPosition(new Point(1, 0)).get().isGround());
        assertTrue(aerialMap.getMap().get(0).getBiomesList().isEmpty());
    }

    @Test
    public void echoMapOUT_OF_RANGE() {
        aerialMap.mapEcho(drone, Directions.SOUTH, 53, "OUT_OF_RANGE");
        assertEquals(53, aerialMap.getMap().size());
        for (int i = 1; i <= aerialMap.getMap().size(); i++) {
            assertTrue(aerialMap.getTileByPosition(new Point(0, -i)).isPresent());
            assertEquals(Biomes.OCEAN, aerialMap.getMap().get(i - 1).getBiomesList().get(0));
        }
    }

    @Test
    public void echoMapOUT_OF_RANGEAtRange0() {
        aerialMap.mapEcho(drone, Directions.SOUTH, 0, "OUT_OF_RANGE");
        assertEquals(1, aerialMap.getMap().size());
        assertTrue(aerialMap.getTileByPosition(drone.getPosition()).isPresent());
        assertEquals(Biomes.OCEAN, aerialMap.getMap().get(0).getBiomesList().get(0));
    }

    @Test
    public void scanMap() {
        List<Biomes> biomesList = new ArrayList<>();
        List<String> creeksList = new ArrayList<>();

        biomesList.add(Biomes.GLACIER);
        biomesList.add(Biomes.ALPINE);

        aerialMap.mapScan(new Point(-2, 12), biomesList, creeksList, "");
        assertEquals(true, aerialMap.getMap().get(0).isGround());
        assertEquals(biomesList, aerialMap.getMap().get(0).getBiomesList());
    }

    @Test
    public void scanMapWithCreek() {
        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.GLACIER);
        List<String> creeksList = new ArrayList<>();

        creeksList.add("9d63bf93-484a-489a-9ac6-e81377734ef5");

        aerialMap.mapScan(new Point(-2, 12), biomesList, creeksList, "");
        assertEquals(true, aerialMap.getMap().get(0).isGround());
        assertEquals("9d63bf93-484a-489a-9ac6-e81377734ef5", aerialMap.getCreeksList().get(0).idCreek());
        assertEquals("9d63bf93-484a-489a-9ac6-e81377734ef5", aerialMap.getMap().get(0).idCreek());
    }

    @Test
    public void scanMapWithSite() {
        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.GLACIER);
        List<String> creeksList = new ArrayList<>();

        aerialMap.mapScan(new Point(-2, 12), biomesList, creeksList, "7g10vb64-922b-clc2-a209-i48168955uf8");
        assertEquals(true, aerialMap.getMap().get(0).isGround());
        assertEquals(true, aerialMap.getCreeksList().isEmpty());
        assertEquals("7g10vb64-922b-clc2-a209-i48168955uf8", aerialMap.getEmergencySite().get().idEmergencySite());
        assertTrue(aerialMap.getEmergencySite().isPresent());
    }

    @Test
    public void scanMapOnOcean() {
        List<Biomes> biomesList = new ArrayList<>();
        biomesList.add(Biomes.OCEAN);
        List<String> creeksList = new ArrayList<>();

        aerialMap.mapScan(new Point(-2, 12), biomesList, creeksList, "");
        assertEquals(true, aerialMap.getCreeksList().isEmpty());
        assertEquals(false, aerialMap.getEmergencySite().isPresent());
        assertEquals(Biomes.OCEAN, aerialMap.getMap().get(0).getBiomesList().get(0));
    }

    @Test
    public void addTile() {
        Tile tile = new Tile(new Point(2, 4), false, false, null, null, new ArrayList<>());
        aerialMap.addTile(tile);
        assertEquals(tile, aerialMap.getMap().get(0));
        aerialMap.addTile(null);
        assertEquals(1, aerialMap.getMap().size());
    }

    @Test
    public void getTileByPosition() {
        Tile tile = new Tile(new Point(1, 3), false, false, null, null, new ArrayList<>());
        aerialMap.addTile(tile);
        assertEquals(Optional.of(tile), aerialMap.getTileByPosition(new Point(1, 3)));
        assertEquals(Optional.empty(), aerialMap.getTileByPosition(new Point(12, 45)));
    }

}
