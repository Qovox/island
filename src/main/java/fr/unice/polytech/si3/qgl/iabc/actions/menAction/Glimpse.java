package fr.unice.polytech.si3.qgl.iabc.actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.Action;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Biomes;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maven
 * @version 1.2
 *          This class implements the glimpse action behavior.
 */
public class Glimpse extends Action {

    //the name of the action.
    private final String name;
    //the action.
    private JSONObject action;
    //the direction of the action.
    private Directions direction;
    //the range of the action.
    private int range;

    /**
     * {"action":"glimpse","parameters":{"direction":"N","range":2}}
     * Constructor of the class Glimpse.
     *
     * @param direction the direction to glimpse at.
     * @param range     the range of the glimpse.
     */
    public Glimpse(Directions direction, int range) {
        name = "glimpse";
        JSONObject parameters = new JSONObject();
        parameters.put("direction", direction);
        parameters.put("range", range);
        action = new JSONObject();
        action.put("action", name);
        action.put("parameters", parameters);
        this.direction = direction;
        this.range = range;
    }

    /**
     * This method allows this action to process his own results.
     *
     * @param result  the results from the last action.
     * @param context the previous context to update.
     * @param men     the previous men to update.
     */
    @Override
    public void aftermath(String result, Context context, Men men) {
        super.aftermath(result, context, men);
        List<List<Biomes>> biomesGlimpsed = jsonParser(result);
        men.glimpseUpdate(direction, range, biomesGlimpsed);
    }

    /**
     * This method allows to parse the Json.
     *
     * @param result the result of the realized action.
     * @return the list of the glimpsed biomes.
     */
    public List<List<Biomes>> jsonParser(String result) {
        JSONObject stringToJSON = new JSONObject(result);
        JSONObject extrasObjects = stringToJSON.getJSONObject("extras");
        JSONArray jsonReport = extrasObjects.getJSONArray("report");

        List<List<Biomes>> biomesGlimpsed = new ArrayList<>(new ArrayList<>());

        for (int i = 0; i < jsonReport.length(); i++) {
            JSONArray jsonTile = jsonReport.getJSONArray(i);
            biomesGlimpsed.add(new ArrayList<>());
            for (int j = 0; j < jsonTile.length(); j++) {
                Object jsonBiome = jsonTile.get(j);
                if (jsonBiome instanceof String) {
                    biomesGlimpsed.get(i).add(Biomes.getBiomeName((String) jsonBiome));
                } else if (jsonBiome instanceof JSONArray) {
                    for (int k = 0; k < ((JSONArray) jsonBiome).length(); k++) {
                        Object jsonUnknown = ((JSONArray) jsonBiome).get(k);
                        if (jsonUnknown instanceof String) {
                            biomesGlimpsed.get(i).add(Biomes.getBiomeName((String) jsonUnknown));
                        }
                    }
                }
            }
        }

        return biomesGlimpsed;
    }

    /**
     * This method provides the full action name.
     *
     * @return a string representation of the object action.
     */
    @Override
    public String toString() {
        return action.toString();
    }

    //=================
    //Getter and Setter
    //=================

    /**
     * Getter of the action name.
     *
     * @return the name of the action.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Getter of the direction.
     *
     * @return the direction used to do this action.
     */
    public Directions getDirection() {
        return direction;
    }

    /**
     * Getter of the range.
     *
     * @return the range which to glimpse.
     */
    public int getRange() {
        return range;
    }

}
