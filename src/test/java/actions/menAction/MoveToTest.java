package actions.menAction;

import fr.unice.polytech.si3.qgl.iabc.actions.menAction.MoveTo;
import fr.unice.polytech.si3.qgl.iabc.exploration.Context;
import fr.unice.polytech.si3.qgl.iabc.exploration.Drone;
import fr.unice.polytech.si3.qgl.iabc.exploration.Inventory;
import fr.unice.polytech.si3.qgl.iabc.exploration.Men;
import fr.unice.polytech.si3.qgl.iabc.map.Directions;
import fr.unice.polytech.si3.qgl.iabc.map.Radar;
import fr.unice.polytech.si3.qgl.iabc.map.TerrestrialMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MoveToTest {

    private Men men;
    private Context context;
    private Drone drone;

    @Before
    public void setUp() {
        String stringContext = "{\"men\": 10,\"budget\":20000,\"contracts\":" +
                "[{ \"amount\":6000,\"resource\":\"WOOD\"}," +
                "{ \"amount\":2000,\"resource\":\"GLASS\"}]," +
                "\"heading\":\"W\"}";
        men = new Men(new Point(), new Inventory(new HashMap<>()), new Radar());
        drone = new Drone();
        context = new Context(stringContext, drone);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void moveToToString() {
        MoveTo moveto = new MoveTo(Directions.SOUTH);
        String MoveToToString = "{\"action\":\"move_to\"," +
                "\"parameters\":{\"direction\":\"S\"}}";
        String BadMoveToToString = "{\"action\":\"move to\"," +
                "\"parameters\":{\"direction\":\"E\"}}";
        assertEquals(MoveToToString, moveto.toString());
        assertNotEquals(BadMoveToToString, moveto.toString());
        assertEquals("move_to", moveto.getName());
        assertEquals(Directions.SOUTH, moveto.getDirection());
        assertNotEquals(MoveToToString, new MoveTo(Directions.EAST).toString());
    }

    @Test
    public void movetoAftermath() {
        String result = "{\"cost\":3}";
        new MoveTo(Directions.NORTH).aftermath(result, context, men);
        assertEquals(19997, context.getBudget().getBudgetLeft());
        assertEquals(new Point(0, 1), men.getPosition());

    }


}
