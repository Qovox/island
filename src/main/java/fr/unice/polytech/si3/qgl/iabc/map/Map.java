package fr.unice.polytech.si3.qgl.iabc.map;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Maven
 * @version 1.1
 *          Contains data shared by the terrestrial and aerial map
 */
public class Map {

    //the grid map.
    private List<Tile> map;
    //the terrestrial map. (grid too)
    private List<Tile> terrestrialMap;

    /**
     * Constructor of Map.
     */
    public Map() {
        map = new ArrayList<>();
        terrestrialMap = new ArrayList<>();
    }

    /**
     * This method allows to manually add tile to the aerialMap.
     * Also, it fills the terrestrial map each time it can.
     *
     * @param tile the tile to be added.
     */
    public void addTile(Tile tile) {
        if (tile != null && !map.contains(tile)) {
            if (tile.getBiomesList().size() == 1) {
                int x = (int) tile.getPosition().getX() * 3;
                int y = (int) tile.getPosition().getY() * 3;
                for (int i = y; i < y + 3; i++) {
                    for (int j = x; j < x + 3; j++) {
                        if (tile.idCreek() == null && tile.idEmergencySite() == null) {
                            terrestrialMap.add(new Tile(false, false, new Point(j, i), tile.getBiomesList()));
                        } else {
                            terrestrialMap.add(new Tile(new Point(j, i), tile.isGround(), tile.isDanger(),
                                    null, null, tile.getBiomesList()));
                        }
                    }
                }
            }
            map.add(tile);
        }
    }

    /**
     * This method searches a tile in the map with given x and y position.
     *
     * @param position the coordinates.
     * @return the searched tile if it exists, null otherwise.
     */
    public Optional<Tile> getTileByPosition(Point position) {
        if (!getMap().isEmpty()) {
            for (Tile tile : map) {
                if (Double.compare(position.getX(), tile.getPosition().getX()) == 0 &&
                        Double.compare(position.getY(), tile.getPosition().getY()) == 0) {
                    return Optional.of(tile);
                }
            }
        }
        return Optional.empty();
    }

    /**
     * Getter of the map.
     *
     * @return map the current map.
     */
    public List<Tile> getMap() {
        return map;
    }

    /**
     * Getter of the terrestrial map.
     *
     * @return the grid map.
     */
    public List<Tile> getTerrestrialMap() {
        return terrestrialMap;
    }

}
