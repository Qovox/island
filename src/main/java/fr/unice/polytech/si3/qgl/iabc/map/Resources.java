package fr.unice.polytech.si3.qgl.iabc.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.unice.polytech.si3.qgl.iabc.map.Biomes.*;

/**
 * @author Maven
 * @version 1.0
 *          An enumeration of all available resources.
 */
public enum Resources {

    FISH(false, OCEAN, LAKE), FLOWER(false, MANGROVE, ALPINE, GLACIER), FRUITS(false, TROPICAL_RAIN_FOREST, TROPICAL_SEASONAL_FOREST),
    FUR(false, GRASSLAND, TEMPERATE_RAIN_FOREST, SHRUBLAND), ORE(false, TEMPERATE_DESERT, ALPINE, SUB_TROPICAL_DESERT),
    QUARTZ(false, BEACH, TEMPERATE_DESERT, SUB_TROPICAL_DESERT), SUGAR_CANE(false, TROPICAL_RAIN_FOREST, TROPICAL_SEASONAL_FOREST),
    WOOD(false, MANGROVE, TROPICAL_RAIN_FOREST, TROPICAL_SEASONAL_FOREST, TEMPERATE_DECIDUOUS_FOREST, TEMPERATE_RAIN_FOREST, TAIGA),

    GLASS(true, 12, 7, QUARTZ, WOOD), INGOT(true, 7, 7, ORE, WOOD), LEATHER(true, 5, 0, FUR),
    PLANK(true, 3, 0, WOOD), RUM(true, 12, 3, SUGAR_CANE, FRUITS);

    //to know whether a resource is a manufactured one or not.
    private boolean manufactured;
    //the list of biomes.
    private List<Biomes> biomesList;
    //the recipe of a manufactured resource.
    private Map<Resources, Integer> recipe;

    /**
     * Constructor of a resource.
     */
    Resources(boolean manufactured, Biomes... biomes) {
        this.manufactured = manufactured;
        biomesList = new ArrayList<>();
        for (Biomes biome : biomes) {
            biomesList.add(biome);
        }
    }

    Resources(boolean manufactured, int amount1, int amount2, Resources... resources) {
        this.manufactured = manufactured;
        recipe = new HashMap<>();
        recipe.put(resources[0], amount1);
        if (amount2 != 0)
            recipe.put(resources[1], amount2);
    }

    /**
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return this.name();
    }

    /**
     * This method allows to get the name of the resource.
     *
     * @param resource the biome to analyze.
     * @return the name of the pointed resource.
     */
    public static Resources getResourceName(String resource) {
        for (Resources resources : Resources.values()) {
            if (resources.toString().equals(resource)) {
                return resources;
            }
        }
        return null;
    }

    /**
     * Getter of the recipe.
     */
    public Map<Resources, Integer> getRecipe() {
        return recipe;
    }

    /**
     * This method allows to know whether it's a manufactured resource or not.
     *
     * @return true if the resource is manufactured, false otherwise
     */
    public boolean isManufactured() {
        return manufactured;
    }

    /**
     * Getter of the list of biomes.
     *
     * @return the list of biomes.
     */
    public List<Biomes> getBiomesList() {
        return biomesList;
    }

}
