package fr.unice.polytech.si3.qgl.iabc.map;

/**
 * Enumeration of the different available amount in the Island game.
 * We have : low, medium or high quantity of the different resources.
 */
public enum Amount {

    HIGH, LOW, MEDIUM;

    /**
     * @return a string representation of the object name.
     */
    @Override
    public String toString() {
        return this.name();
    }

    /**
     * This method allows an Enum to get its real name.
     *
     * @return the real name of the Enum.
     */
    public static Amount getAmountName(String amount) {
        for (Amount amounts : Amount.values()) {
            if (amounts.toString().equals(amount)) {
                return amounts;
            }
        }
        return null;
    }
}
