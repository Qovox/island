package fr.unice.polytech.si3.qgl.iabc.map;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Maven
 * @version 1.1
 *          This class represents a terrestrial map where the man will move.
 */
public class TerrestrialMap extends Map {

    //the list of tiles representing the grid map.
    private List<Tile> tileList;
    //the list of biomes.
    private List<Biomes> biomesList;

    /**
     * Constructor of the terrestrial map.
     */
    public TerrestrialMap() {
        tileList = new ArrayList<>();
        biomesList = new ArrayList<>();
    }

    /**
     * Constructor for a terrestrial map.
     *
     * @param tileList the terrestrial map built from the aerial phase.
     */
    public TerrestrialMap(List<Tile> tileList) {
        this.tileList = tileList;
        biomesList = new ArrayList<>();
        fillBiomes();
    }

    /**
     * This method allows to fill the list of biomes with the right biomes.
     */
    public void fillBiomes() {
        for (Tile tile : tileList) {
            tile.getBiomesList().stream().filter(biomes -> !biomesList.contains(biomes)).
                    forEach(biomes -> biomesList.add(biomes));
        }
    }

    public void addTile(Tile tile) {
        tileList.add(tile);
    }

    /**
     * This method allows to update the map with the realized glimpse actions.
     *
     * @param menPosition    the position of the men during the action.
     * @param direction      the direction of the glimpse action.
     * @param range          the range of the glimpse action.
     * @param biomesGlimpsed the glimpsed biomes.
     */
    public void glimpseUpdate(Point menPosition, Directions direction, int range, List<List<Biomes>> biomesGlimpsed) {
        int x = (int) menPosition.getX();
        int y = (int) menPosition.getY();
        switch (direction) {
            case NORTH:
                for (int i = 0; i < range; i++) {
                    if (!this.getTileByPosition(new Point(x, y + i)).isPresent())
                        tileList.add(new Tile(false, false, new Point(x, y + i), biomesGlimpsed.get(i)));
                }
                break;
            case EAST:
                for (int i = 0; i < range; i++) {
                    if (!this.getTileByPosition(new Point(x + i, y)).isPresent())
                        tileList.add(new Tile(false, false, new Point(x + i, y), biomesGlimpsed.get(i)));
                }
                break;
            case SOUTH:
                for (int i = 0; i < range; i++) {
                    if (!this.getTileByPosition(new Point(x, y - i)).isPresent())
                        tileList.add(new Tile(false, false, new Point(x, y - i), biomesGlimpsed.get(i)));
                }
                break;
            case WEST:
                for (int i = 0; i < range; i++) {
                    if (!this.getTileByPosition(new Point(x - i, y)).isPresent())
                        tileList.add(new Tile(false, false, new Point(x - i, y), biomesGlimpsed.get(i)));
                }
                break;
        }
    }

    /**
     * This method searches a tile in the map with given x and y position.
     *
     * @param position the coordinates.
     * @return the searched tile if it exists, null otherwise.
     */
    @Override
    public Optional<Tile> getTileByPosition(Point position) {
        if (!tileList.isEmpty()) {
            for (Tile tile : tileList) {
                if (Double.compare(position.getX(), tile.getPosition().getX()) == 0 &&
                        Double.compare(position.getY(), tile.getPosition().getY()) == 0) {
                    return Optional.of(tile);
                }
            }
        }
        return Optional.empty();
    }

    public Optional<Tile> getTileByPositionWithDirection(Point position, Directions direction, int range) {
        int x = (int) position.getX();
        int y = (int) position.getY();
        Optional<Tile> tile;
        switch (direction) {
            case NORTH:
                tile = getTileByPosition(new Point(x, y + range));
                if (tile.isPresent())
                    return tile;
                break;
            case EAST:
                tile = getTileByPosition(new Point(x + range, y));
                if (tile.isPresent())
                    return tile;
                break;
            case SOUTH:
                tile = getTileByPosition(new Point(x, y - range));
                if (tile.isPresent())
                    return tile;
                break;
            case WEST:
                tile = getTileByPosition(new Point(x - range, y));
                if (tile.isPresent())
                    return tile;
                break;
            default:
                return Optional.empty();
        }
        return Optional.empty();
    }

}
